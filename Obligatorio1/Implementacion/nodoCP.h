#ifndef NODOCP_H
#define NODOCP_H
#include "Puntero.h"

template<class P,class T>
class nodoCP{
public:
	nodoCP(){
		prioridad = NULL;
		dato = NULL;
		izq = NULL;
		der = NULL;
	}
	nodoCP(P &p, T &t){
		prioridad = p;
		dato = t;
		izq = new nodoCP<P, T>();
		der = new nodoCP<P, T>();
	}
	~nodoCP(){
		der = NULL;
		izq = NULL;
	}
	nodoCP(P &p, T &t, Puntero<nodoCP<P,T>> iz, Puntero<nodoCP<P,T>> de){
		prioridad = p;
		dato = t;
		izq = iz;
		der = de;
	}
	nodoCP(const nodoCP& n){
		prioridad = n.prioridad;
		dato = n.dato;
		der = n.der;
		izq = n.izq;
	}

	nodoCP<P,T>& operator=(const nodoCP<P,T>& nod){
		prioridad = nod.prioridad;
		dato = nod.dato;
		der = nod.der;
		izq = nod.izq;
		return *this;
	}
	
	friend ostream& operator<<(ostream& out, const nodoCP& n){
		out << n.dato;
		return out;
	}
	P prioridad;
	T dato;
	Puntero<nodoCP> der;
	Puntero<nodoCP> izq;
};
#endif