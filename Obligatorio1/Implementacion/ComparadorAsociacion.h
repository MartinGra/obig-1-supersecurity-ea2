#ifndef COMPARADORASOCIACION_H
#define COMPARADORASOCIACION_H
#include "Puntero.h"
#include "Comparacion.h"
#include "Tupla.h"
template<class D, class R>
class ComparadorAsociacion : public Comparacion <Tupla<D, R> > {
public:
	ComparadorAsociacion(Puntero<Comparador<D>> p){
		pcomp = p;
	}
	
	CompRetorno Comparar(const Tupla<D, R> &d1, const Tupla<D, R> &d2) const override{
		return pcomp->Comparar(d1.ObtenerDato1(), d2.ObtenerDato1());
	}
private:
	Puntero<Comparador<D>> pcomp;
};

#endif