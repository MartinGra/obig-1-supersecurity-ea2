#ifndef CPIMP_CPP
#define CPIMP_CPP
#include "Puntero.h"
#include "CPImp.h"

template<class P, class T>
CPImp<P, T>::CPImp(nat maxS, Puntero<Comparador<P>> comparador){
	heap = Array<Puntero<nodoCP<P, T>>>(maxS + 1); //posicion 0 no se usa
	tope = 0;
	raiz = 1;
	comp = comparador;
}
template<class P, class T>
CPImp<P, T>::CPImp(){
	heap = NULL;
	tope = 0;
	raiz = 1;
}
template<class P, class T>
Puntero<CP<P, T>> CPImp<P,T>::CrearColaVacia() const{
	Puntero<CPImp<P, T>> ret = new CPImp<P, T>(heap.ObtenerLargo(), comp);
	return ret;
}
template<class P, class T>
Puntero<CP<P, T>> CPImp<P,T>::Clon() const{
	Puntero<CPImp<P, T>> hRet = new CPImp<P,T>();
	Array<Puntero<nodoCP<P, T>>> aux = Array<Puntero<nodoCP<P, T>>>(heap.ObtenerLargo());
	aux.Copiar(this->heap, aux, NULL);
	hRet->heap = aux;
	hRet->tope = this->tope;
	hRet->raiz = this->raiz;
	hRet->comp = this->comp;
	return hRet;
}
template<class P, class T>
nat CPImp<P, T>::PosPadre(nat i){
	return floor(i / 2);
}

template<class P, class T>
nat CPImp<P, T>::PosHijoIzq(nat i){
	return 2 * i;
}

template<class P, class T>
nat CPImp<P, T>::PosHijoDer(nat i){
	return (2 * i) + 1;
}

template<class P, class T>
void CPImp<P,T>::Vacia(){
	heap = NULL;
	tope = 0;
	raiz = 1;
}
template<class P, class T>
bool CPImp<P,T>::EsVacia() const{
	return tope == 0; // cuando desencolo hago tope--, cuando encolo hago ++tope
}

template<class P,class T>
bool CPImp<P,T>::EstaLleno(){
	return tope == (heap.ObtenerLargo() -1);
}

template<class P, class T>
void CPImp<P, T>::Encolar(P &p, T &t){
	if (!EstaLleno()){
		Puntero<nodoCP<P, T>> aIns = new nodoCP<P, T>(p, t, NULL, NULL);
		heap[++tope] = aIns;
		Flotar(tope);
	}
}

template<class P, class T>
void CPImp<P,T>::Desencolar(){
	if (!EsVacia()){
		heap[raiz] = heap[tope--];
		Hundir(raiz);
	}
}
template<class P, class T>
T CPImp<P,T>::Frente() const{
	return heap[raiz]->dato;
}
template<class P, class T>
P CPImp<P,T>::PrioridadFrente() const{
	return heap[raiz]->prioridad;
}
template<class P, class T>
bool CPImp<P,T>::EsUnNodoInterno(int pos){
	if (PosHijoIzq(pos) <= tope)return true;
	if (PosHijoDer(pos) <= tope)return true;
	return false;
}

template<class P, class T>
void CPImp<P, T>::Hundir(int pos){
	if (EsUnNodoInterno(pos)){ 
		int posMenor = PosHijoIzq(pos);
		int posDer = PosHijoDer(pos);
		if (PerteneceAlHeap(posDer)){
			if ((comp->Comparar(heap[posDer]->dato, heap[posMenor]->dato)) == MENOR){
				posMenor = posDer;
			}
		}
		if ((comp->Comparar(heap[pos]->dato, heap[posMenor]->dato)) == MAYOR){
			Intercambio(pos, posMenor);
			Hundir(posMenor);
		}	
	}
}

template<class P, class T>
void CPImp<P, T>::Flotar(int pos){
	int r = raiz;
	if (pos > r){
		int posP = PosPadre(pos);
		if (comp->Comparar(heap[posP]->dato, heap[pos]->dato) == MAYOR){
			Intercambio(posP, pos); 
			Flotar(posP);
		}
	}
}
template<class P,class T>
void CPImp<P, T>::Intercambio(int p1, int p2){
	Puntero<nodoCP<P,T>> temp = heap[p1];
	heap[p1] = heap[p2];
	heap[p2] = temp;
}
template<class P,class T>
bool CPImp<P, T>::PerteneceAlHeap(int n){
	int tamT = tope;
	return n <= tamT;
}

template<class P,class T>
bool CPImp<P, T>::operator<(const CP<P, T> &c) const{
	return comp->Comparar(heap[raiz]->dato,c.Frente()) == MENOR;
}
template<class P, class T>
bool CPImp<P,T>::operator==(const CP<P, T> &c) const{
	bool ret = false;
	Puntero<CP<P, T>> copiaThis = Clon();
	Puntero<CP<P, T>> copiaC = c.Clon();
	while (!copiaThis->EsVacia() && !copiaC->EsVacia()){
		if (copiaThis->Frente() != copiaC->Frente()) ret = false;
		copiaC->Desencolar();
		copiaThis->Desencolar();
		if (copiaC->EsVacia() && copiaThis->EsVacia()) ret = true;
	}
	return ret;
}
/*
template<class P, class T>
CP<P, T>& CPImp<P,T>::operator=(const CP<P, T> &c){
	if (this != &c){
		Vacia();
		Puntero<CPImp<P, T>> aux = c.Clon();
		while (!aux->EsVacia()){
			Encolar(aux->PrioridadFrente(), aux->Frente());
			aux->Desencolar();
		}
	}
	return *this;
}
*/
#endif