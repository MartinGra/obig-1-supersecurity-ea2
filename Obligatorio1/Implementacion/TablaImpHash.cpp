#ifndef TABLAIMPHASH_CPP
#define TABLAIMPHASH_CPP
#include "TablaImpHash.h"

template<class D, class R>
void TablaImpHash<D,R>:: Agregar(const D&d, const R&r){	
	Asociacion<D, R> as = Asociacion<D, R>(d, r);
	nat index = fh->CodigoDeHash(d) % hash->getArray().ObtenerLargo();
	hash->Add(index, as);	
}
template<class D, class R>
bool TablaImpHash<D, R>::Eliminar(const D& d){
	if (!EstaDef(d)) return false;
	else {
		Asociacion<D, R> as(d, Recuperar(d));
		return hash->Delete(as);
	}
}

template<class D, class R>
R TablaImpHash<D, R>::Recuperar(const D& d)const{
	if (!EstaDef(d)){
		return NULL;
	}
	else{
		Asociacion<D, R> as = Asociacion<D, R>(d);
		return hash->Recover(as).GetRango();
	}
}
template<class D,class R>
void TablaImpHash<D, R>::imprimirTab(){
	hash->printHash();
}
template<class D, class R>
bool TablaImpHash<D, R>::EstaDef(const D& d) const{
	Asociacion<D, R> as(d);
	return hash->isMember(as);
}

template<class D, class R>
Puntero<Tabla<D, R>> TablaImpHash<D, R>::Clon() const{
	Puntero<TablaImpHash<D, R>> tb = new TablaImpHash<D, R>(tam, fh);
	tb->hash = this->hash; 
	return tb;
}
template<class D, class R>
Puntero<Tabla<D, R>> TablaImpHash<D, R>::TablaVacia() const{
	return new TablaImpHash<D,R>(tam,fh); 
}
template<class D, class R>
void TablaImpHash<D, R>::Vaciar() {
	hash = NULL;
	fh = NULL;
}
template<class D, class R>
bool TablaImpHash<D, R>::esVacia() const{
	return (hash->getCantElementos()) == 0;
}
#endif