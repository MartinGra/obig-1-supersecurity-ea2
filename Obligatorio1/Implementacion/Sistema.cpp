﻿#include "Sistema.h"
#include <cstdlib>
#include <iomanip>
Sistema::Sistema(nat MAX_BARRIOS, nat MAX_CLIENTES){
	//Funcions Hash
	Puntero<CadenaFuncionHash> cf = new CadenaFuncionHash();
	Puntero<NaturalFuncionHash> nf = new NaturalFuncionHash();
	Puntero<FuncionHashVertice> fhVer = new FuncionHashVertice(cf);
	//Puntero<FuncionHashBarrio> fhBarr = new FuncionHashBarrio(cf); //para colaPr

	//Funcion Hash asociacion
	Puntero<FuncionHashAsociacion<nat, Puntero<Cliente>>> fhNatCliente = new FuncionHashAsociacion<nat, Puntero<Cliente>>(nf);
	Puntero<FuncionHashAsociacion<nat, Puntero<Vertice<Barrio>>>> fhNatVertice = new FuncionHashAsociacion <nat, Puntero<Vertice<Barrio>>>(nf);
	Puntero<FuncionHashAsociacion<Puntero<Vertice<Barrio>>, nat>> fhVerticeNat = new FuncionHashAsociacion <Puntero<Vertice<Barrio>>, nat>(fhVer);
	//Puntero<FuncionHashAsociacion<Puntero<Barrio>, nat>> fhBarrioNat = new FuncionHashAsociacion<Puntero<Barrio>, nat>(fhBarr);

	//Atributos
	avlClientes = new AVL<Puntero<Cliente>>();
	avlBarrios = new AVL<Puntero<Barrio>>();
	hashClientes = new TablaImpHash<nat, Puntero<Cliente>>(MAX_CLIENTES, fhNatCliente);
	grafoBarrios = new GrafoImpMatriz<Puntero<Vertice<Barrio>>, AristaConexion>(MAX_BARRIOS, fhVerticeNat, fhNatVertice);
	this->MAX_BARRIOS = MAX_BARRIOS;
	this->MAX_CLIENTES = MAX_CLIENTES;	

	Puntero<Comparacion<Puntero<Barrio>>> comp = new ComparadorBarrioCantCliente();
	Puntero<Comparador<Puntero<Barrio>>> pCompBarrio = new Comparador<Puntero<Barrio>>(comp);
	cpBarriosPorClientes = new CPImp<Puntero<Barrio>, Puntero<Barrio>>(MAX_BARRIOS, pCompBarrio);
}

// Tipo 1

TipoRetorno Sistema::IngresoBarrio(Cadena nombreBarrio, nat cantidadCamaras, nat promedioHurtos)
{
	TipoRetorno ret = ERROR;
	Puntero<Barrio> nuevoBarrio = new Barrio(nombreBarrio, cantidadCamaras, promedioHurtos);
	Puntero<Vertice<Barrio>> verticeBarrio = new Vertice<Barrio>(nuevoBarrio);
	int cantB = this->MAX_BARRIOS;
	if (avlBarrios->getCantNodos() < cantB){
		if (!avlBarrios->pertenece(nuevoBarrio)){ 
				avlBarrios->Insertar(nuevoBarrio);
				grafoBarrios->agregarVertice(verticeBarrio);
				cpBarriosPorClientes->Encolar(nuevoBarrio,nuevoBarrio);
				ret = OK;
		}
	}
	return ret;
}

TipoRetorno Sistema::IngresoCliente(nat ciCliente, Cadena nombreCliente, nat fechaRegCliente, Cadena nombreBarrio, Cadena direccion)
{
	TipoRetorno ret = ERROR;
	Puntero<Cliente> nuevoCliente = new Cliente(ciCliente,nombreCliente,fechaRegCliente,nombreBarrio,direccion);
	int cantC = this->MAX_CLIENTES;
	if (avlClientes->getCantNodos() < cantC){
		if (!avlClientes->pertenece(nuevoCliente)){ 
			Puntero<Barrio> barrTemp = new Barrio(nombreBarrio, 0, 0);
			if (avlBarrios->pertenece(barrTemp)){
				avlClientes->Insertar(nuevoCliente);
				hashClientes->Agregar(ciCliente,nuevoCliente);
				barrTemp = avlBarrios->obtener(barrTemp);
				barrTemp->ObtenerClientes()->Insertar(nuevoCliente);
				ret = OK;
			}
		}
	}
	return ret;
}

Tupla<TipoRetorno, pCliente> Sistema::ConsultaCliente(nat ciCliente)
{
	if (hashClientes->EstaDef(ciCliente)){
		return TUPLA(OK, hashClientes->Recuperar(ciCliente)); // O(1) promedio
	}else{
		return TUPLA(ERROR, nullptr);
	}
}

Tupla<TipoRetorno, Iterador<pBarrio>> Sistema::ListadoBarrios()
{
	if (avlBarrios->esVacio()){
		return TUPLA(ERROR, nullptr);
	}else{
		return TUPLA(OK, avlBarrios->ObtenerIterador());
	}
}

Tupla<TipoRetorno, Iterador<pCliente>> Sistema::ListadoClientes()
{
	if (avlClientes->esVacio()){
		return TUPLA(ERROR, nullptr);
	}
	else{
		return TUPLA(OK, avlClientes->ObtenerIterador());
	}
}

TipoRetorno Sistema::IngresoConexion(Cadena nombreBarrioOrigen, Cadena nombreBarrioDestino, nat distancia, nat tiempo)
{	
	TipoRetorno ret = ERROR;
	Puntero<Barrio> orAux = new Barrio(nombreBarrioOrigen);
	Puntero<Barrio> desAux = new Barrio(nombreBarrioDestino);
	Puntero<Vertice<Barrio>> origen = new Vertice<Barrio>(orAux);
	Puntero<Vertice<Barrio>> destino = new Vertice<Barrio>(desAux);
	nat posOr = grafoBarrios->getVertice(origen);
	nat posDes = grafoBarrios->getVertice(destino);
	origen = grafoBarrios->getVertice(posOr); 
	destino = grafoBarrios->getVertice(posDes);
	if (nombreBarrioOrigen != nombreBarrioDestino){ // Agrega solo cuando origen != destino
		if (grafoBarrios->existeVertice(posOr) && grafoBarrios->existeVertice(posDes)){
			AristaConexion arista = AristaConexion(origen, destino, distancia, tiempo);
			nat cantMaxVert = grafoBarrios->getMaxVertices();
			if (posOr < cantMaxVert && posDes < cantMaxVert){
				grafoBarrios->agregarArista(arista, arista.origen, arista.destino); 
				ret = OK;
			}
		}
	}
	return ret;
}


Tupla<TipoRetorno, pRecorrido> Sistema::CaminoMasRapido(Cadena nombreClienteOrigen, Cadena nombreClienteDestino)
{
	Iterador<Puntero<Cliente>> it = avlClientes->ObtenerIterador();
	Puntero<Barrio> origen = NULL;
	Puntero<Barrio> destino = NULL;
	Puntero<Vertice<Barrio>> vOr, vDes;
	TipoRetorno ret = ERROR;
	Puntero<Recorrido> rec = nullptr;

	//Cargo vertices de los barrios origen y destino
	while (it.HayElemento()){
		if (it.ElementoActual()->ObtenerNombreCliente() == nombreClienteOrigen){
			origen = avlBarrios->obtener(new Barrio(it.ElementoActual()->ObtenerNombreBarrio()));
			vOr = new Vertice<Barrio>(origen);
		}
		if (it.ElementoActual()->ObtenerNombreCliente() == nombreClienteDestino){
			destino = avlBarrios->obtener(new Barrio(it.ElementoActual()->ObtenerNombreBarrio()));
			vDes = new Vertice<Barrio>(destino);
		}
		it.Avanzar();
	}
	if (origen && destino){ // encontre clientes y respectivos barrios (sino estan en NULL al crear las variables)
		Matriz<Puntero<AristaConexion>> matrizPodada = matrizAristasMenores(grafoBarrios->getMatrizAdy(),true);
		Matriz<Puntero<Lista<Puntero<AristaConexion>>>> caminosMasRapidos = floydLista(matrizPodada,true);
		int indiceOrigen = grafoBarrios->getVertice(vOr);
		int indiceDestino = grafoBarrios->getVertice(vDes);

		if (indiceOrigen == indiceDestino){ //retorno el recorrido vacio, de origen barrio origen
			rec = new Recorrido(origen); 
		}else if (caminosMasRapidos[indiceOrigen][indiceDestino]){ //hay caminos desde origen a destino
			rec = new Recorrido(origen);
			rec->setTraslados(caminosMasRapidos[indiceOrigen][indiceDestino]);
			cout <<rec;
			ret = OK;
		} //si no hay el recorrido queda en nullptr
	}

	return TUPLA(ret, rec);
}
//Esta funcion se utliza para obtener la menor arista de una lista de aristas, segun un parametro {tiempo o distancia}
Puntero<AristaConexion> Sistema::obtenerMinimaArista(Puntero<Lista<AristaConexion>> l,bool priorizaTiempo){
	Puntero<NodoLista<AristaConexion>> aux = l->getRoot();
	AristaConexion ret = aux->obj;
	int criterio;
	int critTemp;
	while (aux){
		if (priorizaTiempo){
			criterio = aux->obj.tiempo;
			critTemp = ret.tiempo;
		}else{
			criterio = aux->obj.distancia;
			critTemp = ret.distancia;
		}
		if (criterio < critTemp){
			ret = aux->obj;
		}
		aux = aux->sig;
	}
	return new AristaConexion(ret);
}
//A partir de la martiz de adyacencia del multi grafo, retornamos la matriz de aristas con menor peso (parametro {tiempo o distancia})
Matriz<Puntero<AristaConexion>> Sistema:: matrizAristasMenores(Matriz<Puntero<Lista<AristaConexion>>> mat,bool priorizaTiempo){
	Matriz<Puntero<AristaConexion>> ret = Matriz<Puntero<AristaConexion>>(mat.ObtenerLargo());
	int cn = mat.ObtenerLargo();
	for (int i = 0; i < cn; i++){
		for (int j = 0; j < cn; j++){
			if (mat[i][j]){
				ret[i][j] = obtenerMinimaArista(mat[i][j],priorizaTiempo);
			}else{
				ret[i][j] = NULL;
			}
		}
	}
	return ret;
}

// Tipo 2

TipoRetorno Sistema::BajaCliente(nat ciCliente)
{
	Puntero<Barrio> bGraf, bAVL, bCola;
	TipoRetorno ret = ERROR;
	if (hashClientes->EstaDef(ciCliente)){ 
		Puntero<Cliente> cliente = hashClientes->Recuperar(ciCliente);
		Cadena nombreCliente = cliente->ObtenerNombreCliente();
		int indice = grafoBarrios->getVertice(new Vertice<Barrio>(new Barrio(cliente->ObtenerNombreBarrio(),0,0)));
		Puntero<Vertice<Barrio>> v = grafoBarrios->getVertice(indice);
		
		Cadena nombreBarr = v->getVertice()->ObtenerNombreBarrio();
		bAVL = avlBarrios->obtener(new Barrio(nombreBarr));
		hashClientes->Eliminar(ciCliente); //Borro del hash
		avlClientes->Borrar(new Cliente(ciCliente, nombreCliente, 0, nombreBarr, "")); // borro del avl clientes
		bAVL->ObtenerClientes()->Borrar(cliente); //Borro del avlBarrios
		bGraf = v->getVertice();
		bGraf->ObtenerClientes()->Borrar(cliente); //borro del barrio del grafo
		//Falta borrar en cola de prioridad 
		ret = OK;
	}
	return ret;
}

Tupla<TipoRetorno, pBarrio> Sistema::ConsultaBarrio(Cadena nombreBarrio)
{
	Puntero<Barrio> pb = new Barrio(nombreBarrio, 0, 0);
	Puntero<Vertice<Barrio>> v = new Vertice<Barrio>(pb);
	nat indice = grafoBarrios->getVertice(v);
	if (indice != -1){
		Puntero<Vertice<Barrio>> pv = grafoBarrios->getVertice(indice);
		Iterador<Puntero<Cliente>> it = pv->getVertice()->ObtenerClientesPorDireccion();
		return TUPLA(OK, pv->getVertice());
	}else return TUPLA(ERROR, nullptr);
}

Tupla<TipoRetorno, pBarrio> Sistema::BarrioConMasClientes()
{
	if (cpBarriosPorClientes->EsVacia()){
		return TUPLA(ERROR, nullptr);
	}
	else{
		return TUPLA(OK, cpBarriosPorClientes->Frente());
	}
}

Tupla<TipoRetorno, pRecorrido> Sistema::CaminoMenorDistancia(Cadena nombreBarrioOrigen, Cadena nombreBarrioDestino)
{
	Puntero<Barrio> origen = NULL;
	Puntero<Barrio> destino = NULL;
	Puntero<Vertice<Barrio>> vOr, vDes;
	TipoRetorno ret = ERROR;
	Puntero<Recorrido> rec = nullptr;

	Puntero<Barrio> b = new Barrio(nombreBarrioOrigen);
	//Cargo vertices de los barrios origen y destino
	if (avlBarrios->pertenece(b)){
			origen = avlBarrios->obtener(b);
			vOr = new Vertice<Barrio>(origen);
	}
	b = new Barrio(nombreBarrioDestino);
	if (avlBarrios->pertenece(b)){
		destino = avlBarrios->obtener(b);
		vDes = new Vertice<Barrio>(destino);
	}

	if (origen && destino){ // encontre clientes y respectivos barrios

		Matriz<Puntero<AristaConexion>> matrizPodada = matrizAristasMenores(grafoBarrios->getMatrizAdy(),false);
		Matriz<Puntero<Lista<Puntero<AristaConexion>>>> caminosMasRapidos = floydLista(matrizPodada, false);
		int indiceOrigen = grafoBarrios->getVertice(vOr);
		int indiceDestino = grafoBarrios->getVertice(vDes);

		if (indiceOrigen == indiceDestino){ //retorno el recorrido vacio, de origen
			rec = new Recorrido(origen);
		}
		else if (caminosMasRapidos[indiceOrigen][indiceDestino]){ //hay caminos desde origen a destino
			rec = new Recorrido(origen);
			rec->setTraslados(caminosMasRapidos[indiceOrigen][indiceDestino]);
			cout << rec;
			ret = OK;
		}
	}
	
	return TUPLA(ret, rec);
}

Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> Sistema::CableadoMinimo()
{
	Tupla<nat, nat, Array<Array<int>>> tuplaRet = kruskal(matrizAristasMenores(grafoBarrios->getMatrizAdy(), false));
	nat verticesConectados = tuplaRet.ObtenerDato1();
	nat pesoTotal = tuplaRet.ObtenerDato2();
	Array<Array<int>> arbolRecubrimientoMin = tuplaRet.ObtenerDato3();
	Puntero<Lista<Tupla<Cadena, Cadena, nat>>> lst = new Lista<Tupla<Cadena, Cadena, nat>>(); 
	nat cn = arbolRecubrimientoMin.ObtenerLargo();
	Puntero<Vertice<Barrio>> vOrTemp, vDesTemp;
	Cadena nombreOr,nombreDes;
	nat cableado;
	Tupla<Cadena, Cadena, nat> cableoTemp;

	if (verticesConectados == grafoBarrios->getCantVertices()){			
		for (nat i = 0; i < cn; i++){
			for (nat j = 0; j < cn; j++){
				if (cn < j - i){ // recorro el triangulo inferior
					if (arbolRecubrimientoMin[i][j] != 0){
						vOrTemp = grafoBarrios->getVertice(i);
						vDesTemp = grafoBarrios->getVertice(j);
						nombreOr = vOrTemp->getVertice()->ObtenerNombreBarrio();
						nombreDes = vDesTemp->getVertice()->ObtenerNombreBarrio();
						cableado = arbolRecubrimientoMin[i][j];
						cableoTemp = Tupla<Cadena, Cadena, nat>(nombreOr, nombreDes, cableado);
						//cout << "( " << nombreOr << " <-> " << nombreDes << " ) - cableado = " << cableado << endl;
						lst->InsertarFin(cableoTemp);
					}
				}
			}
		}
		// cout << endl << "Retorno OK" << " - Peso Total " << pesoTotal<<endl;
		return TUPLA(OK, pesoTotal, lst->ObtenerIterador());
	}
	else{
		return TUPLA(ERROR, 0, nullptr);
	}
}
//Metodo para calcular costo (tiempo total) de una lista
int obtenerTiempoTotal(Puntero<Lista<Puntero<AristaConexion>>> lst){
	//PRE: existe root, valida if de floyd
	int tiempo = 0;
	Iterador<Puntero<AristaConexion>> it = lst->ObtenerIterador();
	while (it.HayElemento()){
		tiempo += it.ElementoActual()->tiempo;
		it.Avanzar();
	}
	return tiempo;
}
//Metodo para calcular costo (distancia total) de lista
int obtenerDistanciaTotal(Puntero<Lista<Puntero<AristaConexion>>> lst){
	//PRE: existe root, valida if de floyd
	int ret = 0;
	Iterador<Puntero<AristaConexion>> it = lst->ObtenerIterador();
	while (it.HayElemento()){
		ret+= it.ElementoActual()->distancia;
		it.Avanzar();
	}
	return ret;
}
//Kruskal retorna una tupla que contiene la cantidad de vertices conectados, el peso total, y el arbol de recubrimiento
Tupla<nat, nat, Array<Array<int>>> Sistema::kruskal(Matriz<Puntero<AristaConexion>> adyacencia){
	int cn = MAX_BARRIOS;
	int verticesConectados = 1;
	int pesoTotal = 0;
	Array<Array<int>> arbol = Array<Array<int>>(cn);
	Array<int> pertenece = Array<int>(cn);
	for (int i = 0; i < cn; i++){
		arbol[i] = Array<int>(cn,0);
		pertenece[i] = i;
	}
	int nodoA = 0;
	int nodoB = 0;
	int vertices = 1;
	while (vertices < cn){
		int min = 32767;
		for (int i = 0; i < cn; i++)
			for (int j = 0; j < cn; j++)
				if (adyacencia[i][j])
					if (min > adyacencia[i][j]->distancia && adyacencia[i][j]->distancia != 0 && pertenece[i] != pertenece[j]){
						min = adyacencia[i][j]->distancia;
						nodoA = i;
						nodoB = j;
					}
		if (pertenece[nodoA] != pertenece[nodoB]){
			verticesConectados++;
			pesoTotal += min;
			arbol[nodoA][nodoB] = min;
			arbol[nodoB][nodoA] = min;
		
			int temp = pertenece[nodoB];
			pertenece[nodoB] = pertenece[nodoA];
			for (int k = 0; k < cn; k++)
				if (pertenece[k] == temp)
					pertenece[k] = pertenece[nodoA];
		}
		vertices++;
	}
	Tupla<nat, nat, Array<Array<int>>> retorno = Tupla<nat, nat, Array<Array<int>>>(verticesConectados, pesoTotal, arbol);
	return retorno;
	}

// Este floyd recibe una matrziz con las aristas con menor peso que comunican los barrios que son indice
Matriz<Puntero<Lista<Puntero<AristaConexion>>>> Sistema::floydLista(Matriz<Puntero<AristaConexion>> path, bool menorTiempo){
	int cn = path.ObtenerLargo();
	Matriz<Puntero<Lista<Puntero<AristaConexion>>>> ret = Matriz<Puntero<Lista<Puntero<AristaConexion>>>>(cn);
	Puntero<Barrio> unB = new Barrio("",0,0);
	Puntero<Vertice<Barrio>> v = new Vertice<Barrio>(unB);

	for (int i = 0; i < cn; i++){
		for (int j = 0; j < cn; j++){
			if (!path[i][j]) ret[i][j] = new Lista<Puntero<AristaConexion>>(new AristaConexion(v, v, 32767, 32767));
			else ret[i][j] = new Lista<Puntero<AristaConexion>>((path[i][j]));
		}
		ret[i][i] = new Lista<Puntero<AristaConexion>>(new AristaConexion(v, v, 0, 0));
	}

	for (int k = 0; k < cn; k++){
		for (int i = 0; i < cn; i++){
			Puntero<Lista<Puntero<Lista<Puntero<AristaConexion>>>>> caminos = new Lista<Puntero<Lista<Puntero<AristaConexion>>>>();
			for (int j = 0; j < cn; j++){
				if (ret[k][j] && ret[k][j]->getRoot()){
					if (ret[i][k] && ret[i][k]->getRoot()){
						int tAct = obtenerTiempoTotal(ret[i][j]);
						int dAct = obtenerDistanciaTotal(ret[i][j]);
						int lAct = ret[i][j]->largo();
						int tPaso1 = obtenerTiempoTotal(ret[i][k]);
						int dPaso1 = obtenerDistanciaTotal(ret[i][k]);
						int lPaso1 = ret[i][k]->largo();
						int tPaso2 = obtenerTiempoTotal(ret[k][j]);
						int dPaso2 = obtenerDistanciaTotal(ret[k][j]);
						int lPaso2 = ret[k][j]->largo();
						if (menorTiempo){ //Caso orden peso tiempo
							if (tAct > tPaso1 + tPaso2 || 
								tAct == tPaso1 + tPaso2 && dAct > dPaso1 + dPaso2 || 
								tAct == tPaso1 + tPaso2 && dAct == dPaso1 + dPaso2 && lAct > lPaso1 + lPaso2){	
									ret[i][j] = ret[i][k]->concatenarLista(ret[k][j]);
							}		
						}else{ //Caso orden peso distancia
							if (dAct > dPaso1 + dPaso2 ||
								dAct == dPaso1 + dPaso2 && lAct > lPaso1 + lPaso2 ||
								dAct == dPaso1 + dPaso2 && lAct == lPaso1 + lPaso2 && tAct > tPaso1 + tPaso2){
									ret[i][j] = ret[i][k]->concatenarLista(ret[k][j]);
							}
						}// end if
					}// end if
				}// end if
			}// end for
		}// end for
	}// end for

	for (int i = 0; i < cn; i++){
		for (int j = 0; j < cn; j++){
			if (ret[i][j]->getRoot()->obj->tiempo == 32767)
				ret[i][j] = NULL;
		}// end for
		ret[i][i] = new Lista<Puntero<AristaConexion>>(); // Tomo el criterio de una lista unitaria para las conexiones de un barrio consigo mismo
	}// end for
	return ret;
}// end function floydLista

