#ifndef NODOLISTA_H
#define NODOLISTA_H
#include "Puntero.h"
template <class T>
class NodoLista{

public:
	T obj;
	Puntero<NodoLista> sig;
	~NodoLista(){
		sig = NULL;
	}
	NodoLista(){
		obj = NULL;
		sig = NULL;
	}
	NodoLista(const T& x){
		obj = x;
		sig = NULL;
	}
	NodoLista(const T& x, Puntero<NodoLista> node){
		obj = x;
		sig = node;
	}
};

#endif