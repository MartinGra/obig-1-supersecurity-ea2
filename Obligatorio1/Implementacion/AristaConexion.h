#ifndef ARISTACONEXION_H
#define ARISTACONEXION_H
#include "Barrio.h"
#include "Vertice.h"
class AristaConexion{

public:

	AristaConexion(Puntero<Vertice<Barrio>> unO,Puntero<Vertice<Barrio>> unD, int unaD, int unT){
		origen = unO;
		destino = unD;
		distancia = unaD;
		tiempo = unT;
		habilitada = true;
	}
	AristaConexion(){
		origen = new Vertice<Barrio>();
		destino = new Vertice<Barrio>();
		distancia = 0;
		tiempo = 0;
		habilitada = true;
	}
	AristaConexion(const AristaConexion &l){
		origen = l.origen;
		destino = l.destino;
		distancia = l.distancia;
		tiempo = l.tiempo;
		habilitada = l.habilitada;
	}
	~AristaConexion(){
		origen = NULL;
		destino = NULL;
	}

	void Habilitar(){
		habilitada = true;
	}
	void Deshabilitar(){
		habilitada = false;
	}
	int getDist(){ return distancia; }

	bool operator<(const AristaConexion& a) const{
		return this->distancia < a.distancia;
	}
	bool operator==(const AristaConexion& a) const{
		return this->distancia == a.distancia &&this->tiempo == a.tiempo&&this->origen == a.origen&&this->destino == a.destino;
	}
	friend ostream& operator<<(ostream& out, const AristaConexion& a){
			out<<"(" << a.distancia << "," << a.tiempo << ")";
		return out;
	}
	AristaConexion& operator=(const AristaConexion& a) {
		origen = a.origen;
		destino = a.destino;
		distancia = a.distancia;
		tiempo = a.tiempo;
		habilitada = a.habilitada;
		return *this;
	};

	Puntero<Vertice<Barrio>> origen;
	Puntero<Vertice<Barrio>> destino;
	int distancia;
	int tiempo;		
	bool habilitada;
};// end class

#endif
