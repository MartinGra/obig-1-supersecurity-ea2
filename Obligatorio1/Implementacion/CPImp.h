#ifndef CPIMP_H
#define CPIMP_H
#include "Puntero.h"
#include "ColaPrioridad.h"
#include "Array.h"
#include "nodoCP.h"
#include "Comparador.h"
#include "FuncionHashAsociacion.h"
#include <cMath>
template<class P,class T>
class CPImp : public CP<P,T>{
public:
	CPImp(nat, Puntero<Comparador<P>>);
	CPImp();
	~CPImp(){
		comp = NULL;
		heap = NULL;
	}
	Puntero<CP<P, T>> CrearColaVacia() const override;	
	Puntero<CP<P, T>> Clon() const override;

	void Vacia() override;
	bool EsVacia() const override;
	void Encolar(P &p, T &t) override; 
	void Desencolar() override;
	T Frente() const override;
	P PrioridadFrente() const override;
	void Hundir(int pos);
	void Flotar(int pos);
	nat PosPadre(nat i);
	nat PosHijoIzq(nat i);
	nat PosHijoDer(nat i);
	bool PerteneceAlHeap(int n);
	void Intercambio(int p1, int p2);
	bool EstaLleno();
	bool EsUnNodoInterno(int pos);
	Array<Puntero<nodoCP<P, T>>> getHeap() override{ return heap; }
	nat getTope() override{ return tope; }
	bool operator<(const CP<P, T> &c) const override;
	bool operator==(const CP<P, T> &c) const override;
	//CP<P, T>& operator=(const CP<P, T> &c) override;
private:
	Array<Puntero<nodoCP<P,T>>> heap;
	nat tope;
	nat raiz;
	Puntero<Comparador<P>> comp;
};
#include "CPImp.cpp"
#endif