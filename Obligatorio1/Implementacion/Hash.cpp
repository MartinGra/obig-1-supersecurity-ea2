#ifndef HASH_CPP
#define HASH_CPP
#include <math.h>
#include "Hash.h"

template<class T>
Hash<T>::Hash(nat maxSize,Puntero<FuncionHash<T>> p){
	arrH = Array<Puntero<Lista<T>>>(nextPrime(maxSize), NULL);
	f = p;
	celdasOcupadas = 0;
	cantidadElementos = 0;
}

template<class T>
void Hash<T>::Add(nat ind, const T& t){
	cantidadElementos++;
	//double calcFact = ((double)celdasOcupadas) / ((double)arrH.ObtenerLargo());
	//if (calcFact > factorCarga) grow(); //
	if (!arrH[ind]){
		arrH[ind] = new Lista<T>(t);
		celdasOcupadas++;
	}
	else {
		arrH[ind]->Insertar(t);
	}
}

template<class T>
nat Hash<T>::h(const T& dato) const{
	return f->CodigoDeHash(dato) % arrH.ObtenerLargo();
}

template<class T>
T& Hash<T>::Recover(const T d) const{
	//pre : isMember
	return arrH[h(d)]->Obtener(d);
}

template<class T>
bool Hash<T>::Delete(T& d){
	bool borre = false;
	nat id = h(d);
	if (arrH[id]){
		borre = arrH[id]->Borrar(d);
	}
	if(borre){
		cantidadElementos--;
	}
	return borre;
}

template<class T>
bool Hash<T>::isMember(const T&a)const{
	if (arrH[h(a)]) return arrH[h(a)]->Pertenece(a);
	else return false;
}

/*
template<class T>
void Hash<T>::grow(){
	cout << "crezco"<<endl;
	Array<Puntero<Lista<T>>> nuevo = Array<Puntero<Lista<T>>>(nextPrime(2*arrH.ObtenerLargo()), NULL);
	nuevo.Copiar(arrH, nuevo, NULL);
	arrH = nuevo;
}*/

template <class T>
void Hash<T>::printHash(){
	for (nat i = 0; i < arrH.ObtenerLargo(); i++){
		if (arrH[i]){
			cout << "Bucket " << i << ": ";
			cout<<arrH[i];
		}
		else cout << "Bucket " << i << ": *Empty*"<<endl;	
	}
}

template <class T>
int Hash<T>::nextPrime(int d){
	int sqrtF = floor(sqrt(d));
	bool encontre = false;
	while (!encontre){
		bool primo = true;
		for (int i = 2; i <= sqrtF && primo; i++){
			if (d % i == 0) primo = false;
		}
		(!primo) ? d++ : encontre = true;
	}
	return d;
}
#endif