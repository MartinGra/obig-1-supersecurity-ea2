#ifndef COMPARADORBARRIOCANTCLIENTES_H
#define COMPARADORBARRIOCANTCLIENTES_H
#include "Comparador.h"
#include "Comparacion.h"
#include "Puntero.h"
#include "Barrio.h"
class ComparadorBarrioCantCliente : public Comparacion<Puntero<Barrio>>{

public:
	CompRetorno Comparar(const Puntero<Barrio> &b1, const Puntero<Barrio>& b2) const {
	if (b1->ObtenerClientes()->getCantNodos() < b2->ObtenerClientes()->getCantNodos() || 
		((b1->ObtenerClientes()->getCantNodos() == b2->ObtenerClientes()->getCantNodos()) && b1->ObtenerpromedioHurtos() > b2->ObtenerpromedioHurtos()) ||
		((b1->ObtenerClientes()->getCantNodos() == b2->ObtenerClientes()->getCantNodos()) && (b1->ObtenerpromedioHurtos() == b2->ObtenerpromedioHurtos() && b1->ObtenerNombreBarrio() < b2->ObtenerNombreBarrio())) ){
		return MENOR;
	}else{
		if (b1 == b2){
			return IGUALES;
		}
		else{
			return MAYOR;
		}
	}
	return DISTINTOS;
	
}

};
#endif