#ifndef COMPARADORCLIENTEPORCI_H
#define COMPARADORCLIENTEPORCI_H
#include "Comparador.h"
#include "Comparacion.h"
#include "Puntero.h"
#include "Cliente.h"
class ComparadorClientePorCI : public Comparacion < Puntero<Cliente> > {
public:
	CompRetorno Comparar(const Puntero<Cliente> &c1, const Puntero<Cliente> &c2) const{
		if (c1->ObtenerCiCliente() < c2->ObtenerCiCliente())return MENOR;
		if (c1->ObtenerCiCliente() > c2->ObtenerCiCliente())return MAYOR;
		if (c1->ObtenerCiCliente() == c2->ObtenerCiCliente()) return IGUALES;
		else return DISTINTOS;
	}

};




#endif