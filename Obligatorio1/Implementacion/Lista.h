#ifndef LISTA_H
#define	LISTA_H
#include "NodoLista.h"
#include "Iterable.h"
#include "IteradorLista.h"
#include "Puntero.h"
#include <iostream>
template <class T>
class Lista : public Iterable<T>{

public:
	
	Iterador<T> ObtenerIterador() const override{
		return new IteradorLista<T>(root);
	}
	
	Lista(const T& x){
		root = new NodoLista<T>(x);
	}

	Puntero<Lista> concatenarLista(Puntero<Lista> l1){
		Puntero<NodoLista<T>> aux = root;
		Puntero<Lista> ret = new Lista();

		if (!l1){
			return new Lista(*this);
		}
		while (aux){
			ret->InsertarFin(aux->obj);
			aux = aux->sig;
		}
		aux = l1->root;
		while (aux){
			ret->InsertarFin(aux->obj);
			aux = aux->sig;
		}
		return ret;
	}

	Lista(){
		root = NULL;
	}
	~Lista(){
		root = NULL;
	}
	void Insertar(const T x){
		root = new NodoLista<T>(x, root);
	}
	void InsertarFin(const T x){
		if (!root){
			root = new NodoLista<T>(x);
		}
		else{
			Puntero<NodoLista<T>> aux = root;
			while (aux->sig){
				aux = aux->sig;
			}
			aux->sig = new NodoLista<T>(x);
		}
	}

	bool Borrar(T&e){
		return BorrarRec(root, e);
	}
	bool BorrarRec(Puntero<NodoLista<T>>&p, T&e){
		if (!p) return false;
		if (p->obj == e){
			Puntero<NodoLista<T>> temp = p;
			p = p->sig;
			temp = NULL;
			return true;
		}
		else return BorrarRec(p->sig, e);
	}

	T&Obtener(const T&x){
		return ObtenerRec(root,x);
	}

	T&ObtenerRec(Puntero<NodoLista<T>>&p, const T&x){
		if (p->obj == x) return p->obj;
		else return ObtenerRec(p->sig, x);
	}

	bool Pertenece(const T&x){
		return PerteneceRec(root,x);
	}

	bool PerteneceRec(Puntero<NodoLista<T>> p, const T& x){
		if (!p) return false;
		if (p->obj == x) return true;
		else return PerteneceRec(p->sig, x);
	}

	Puntero<NodoLista<T>> getRoot(){ return root; }
	void setRoot(Puntero<NodoLista<T>> r){root=r; }

	friend ostream& operator<<(ostream& out, const Lista& a){
		Puntero<NodoLista<T>> aux = a.root;
		out << "{";
		while (aux){
			out << aux->obj;
			aux = aux->sig;
		}
		out << "}";
		return out;
	}

	T&ObtenerMenor(){
		if (!root){
			return NULL;
		}
		else{
			Puntero<NodoLista<T>> aux = root;
			T minimo = root->obj;
			while (aux){
				if (aux->obj < minimo){
					minimo = aux->obj;
				}
				aux = aux->sig;
			}
			return minimo;
		}
	}

	int largo(){
		Puntero<NodoLista<T>> aux = root;
		int ret = 0;
		while (aux){
			ret++;
			aux = aux->sig;
		}
		return ret;
	}

private:
	Puntero<NodoLista<T>> root;

}; // end class



#endif