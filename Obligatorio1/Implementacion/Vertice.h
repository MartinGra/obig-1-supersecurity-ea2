#ifndef VERTICE_H
#define VERTICE_H
#include "Puntero.h"

template<class V>
class Vertice{
public:
	Vertice(Puntero<V> unV){
		vertice = unV;
		habilitado = true;
	}
	Vertice(){
		habilitado = true;
		vertice = NULL;
	}
	~Vertice(){
		vertice = NULL;
	}
	void Deshabilitar(){
		habilitado = false;
	}
	void Habilitar(){
		habilitado = true;
	}
	bool getEstado(){
		return habilitado;
	}
	
	Vertice(const Vertice& v){ 
		habilitado = v.habilitado;
		vertice = v.vertice;
	}
	Puntero<V> getVertice() const{
		return vertice;
	}
	friend ostream& operator<<(ostream& out, const Vertice<V>& v){
		out << v.vertice;
		return out;
	}
	bool operator==(const Vertice<V>& v) const {
		return vertice == v.vertice;
	}

	bool operator!=(const Vertice<V>& v) const{
		return !(v == *this);
	}
	bool operator<(const Vertice<V>& v) const{
		return vertice < v.vertice;
	}
	bool operator>(const Vertice<V>& v) const{
		return vertice > v.vertice;
	}
	bool operator<=(const Vertice<V>& v) const{
		return vertice <= v.vertice;
	}
	bool operator>=(const Vertice<V>& v) const{
		return vertice >= v.vertice;
	}
	Vertice<V>& operator=(const Vertice<V>& c){
		habilitado = c.habilitado;
		vertice = c.vertice;
		return *this;
	}
private:
	bool habilitado;
	Puntero <V> vertice;
};


#endif