#ifndef PILAIMP_CPP
#define PILAIMP_CPP
#include "PilaImp.h"
#include <iostream>
using namespace std;
template <class T>
PilaImp<T> ::PilaImp(){
	this->stack = NULL;
}

template<class T>
void PilaImp<T>::Push(const T& e){
	Puntero<NodoLista<T>> aIns = new NodoLista<T>(e, this->stack);
	this->stack = aIns;
}
template<class T>
const T& PilaImp<T> ::Top() const{
	return stack->obj;
}
template<class T>
void PilaImp<T>::Pop(){	
	if (stack) (stack->sig) ? stack = stack->sig : stack = NULL;
}
template<class T>
void PilaImp<T>::Vaciar(){
	while (stack){
		Pop();
	}
	
}
template<class T>
bool PilaImp<T>::esVacia()const{
	return stack == NULL;
}

template<class T>
const T& PilaImp<T>:: ElementoActual() const{
	return this->Top();
}
template < class T > 
void PilaImp<T>:: Avanzar(){
	if (!esVacia())Pop();
}
template<class T>
void PilaImp<T>::Reiniciar(){
	cout << "Este es el reiniciar de pilaimp";
}
template<class T>
bool PilaImp<T>:: HayElemento() const{
	return !esVacia();
}

#endif