#ifndef RECORRIDO_H
#define RECORRIDO_H
#include "IRecorrido.h"

class Recorrido : public IRecorrido{
public:
	Recorrido(Puntero<Barrio> ori){
		traslados = NULL;
		orig =ori;
		tiempoTotal = 0;
		distanciaTotal = 0;
	}
	Recorrido(Puntero<Barrio> ori, nat dist, nat tiemp){
		orig = ori;
		tiempoTotal = tiemp;
		distanciaTotal = dist;
	}
	~Recorrido(){
		traslados = NULL;
		orig = NULL;	
	}
	Cadena ObtenerNombreBarrioOrigen() const override{
		return orig->ObtenerNombreBarrio();
	}
	Iterador<Tupla<Cadena, nat, nat>> ObtenerTraslados() const override{
		return traslados->ObtenerIterador(); 
	}
	nat ObtenerDistanciaTotalRecorrido() const override{
		return tiempoTotal;
	}
	nat ObtenerTiempoTotalRecorrido() const override{
		return distanciaTotal;
	}

    bool operator==(const IRecorrido& r) const override{
		return orig->ObtenerNombreBarrio() == r.ObtenerNombreBarrioOrigen() && tiempoTotal == r.ObtenerTiempoTotalRecorrido() && distanciaTotal == r.ObtenerTiempoTotalRecorrido(); // falta comparar Traslados
	}
	
	friend ostream& operator<<(ostream& out, const Recorrido& r){
		out << "Origen " << r.orig <<  " pasando por: { ";
		Iterador<Tupla<Cadena, nat, nat>> it = r.ObtenerTraslados();
		while (it.HayElemento()){
			 out<< it.ElementoActual().ObtenerDato1() << " ";
			it.Avanzar();
		}	
		out << "} dist= " << r.distanciaTotal<< " tiemp= "<< r.tiempoTotal;
		return out;
	}

	void setTraslados(Puntero<Lista<Puntero<AristaConexion>>>listaConexiones){
		traslados = new Lista<Tupla<Cadena, nat, nat>>();
		Iterador<Puntero<AristaConexion>> it = listaConexiones->ObtenerIterador();
		while (it.HayElemento()){
			Puntero<AristaConexion> temp = it.ElementoActual();
			traslados->InsertarFin(Tupla<Cadena, nat, nat>(temp->destino->getVertice()->ObtenerNombreBarrio(), temp->distancia, temp->tiempo));
			tiempoTotal += temp->tiempo;
			distanciaTotal += temp->distancia;
			it.Avanzar();
		}
	}

private:
	Puntero<Barrio> orig;
	nat tiempoTotal;
	nat distanciaTotal;
	Puntero<Lista<Tupla<Cadena, nat, nat>>> traslados;


};


#endif