#ifndef TABLAIMPHASH_H
#define TABLAIMPHASH_H
#include "Tabla.h"
#include "Puntero.h"
#include "FuncionHashAsociacion.h"
#include "Comparador.h"
#include "ComparadorAsociacion.h"
#include "Tupla.h"
#include "Asociacion.h"
#include "Hash.h"
#include "NodoLista.h"
template <class D, class R>
class TablaImpHash : public Tabla <D, R> {
public:
	TablaImpHash(nat tamanio, Puntero<FuncionHashAsociacion<D,R>> f){
		tam = tamanio;
		fh = f;
		hash = new Hash<Asociacion<D,R>>(tamanio, f);
	}
	~TablaImpHash(){
		hash = NULL;
		fh = NULL;
	}

	 void imprimirTab() override;
	 void Agregar(const D&d, const R&r) override;
	 bool Eliminar(const D&)override;
	 R Recuperar(const D&d)const override;
	 bool EstaDef(const D&) const override;
	 Puntero<Tabla<D, R>> Clon() const override;
	 Puntero<Tabla<D, R>> TablaVacia() const override;
	 void Vaciar() override;
	 bool esVacia() const override;
	 nat natHash(D&d){return hash->h(d); }
	 Puntero<Hash<Asociacion<D, R>>> getHash(){return hash; }
	
	 Tabla& operator=(const TablaImpHash& tab) {
		 tab.hash = hash;
		 tab.fh = fh;
		 tab.tam = tam;
		 return *this;
	 };

	 nat getTam(){ return tam; }
    // Iterador<D> claves() const override;

private:
	Puntero<Hash<Asociacion<D,R>>> hash;
	Puntero<FuncionHashAsociacion<D,R>> fh;
	int tam;
};


#include "TablaImpHash.cpp"

#endif