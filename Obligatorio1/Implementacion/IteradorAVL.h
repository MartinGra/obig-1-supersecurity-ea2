#ifndef ITERADORAVL_H
#define ITERADORAVL_H

#include "Puntero.h"
#include "NodoAVL.h"
#include "PilaImp.h"
#include "Iterador.h"
#include "Iteracion.h"
template <class T>
class IteradorAVL :public Iteracion<T>{
public:
	~IteradorAVL(){
		raiz = NULL;
		auxRaiz = NULL;
		stack = NULL;
	} 

	IteradorAVL(Puntero<NodoAVL<T>> r){
		auxRaiz = r;
		raiz = r;
		stack = new PilaImp<Puntero<NodoAVL<T>>>();
		apilarHastaElMenor(raiz);
	}
	bool HayElemento() const {
		return !stack->esVacia();
	}

	const T& ElementoActual() const{
		assert(HayElemento());
		return stack->Top()->dato;
	}

	void Reiniciar(){
		stack->Vaciar();
		raiz = auxRaiz;
		apilarHastaElMenor(raiz);
	}
	void Avanzar(){
		assert(HayElemento());
		Puntero<NodoAVL<T>> p = stack->Top();
		stack->Pop();
		apilarHastaElMenor(p->hDer);
	}

private:
	Puntero<PilaImp<Puntero<NodoAVL<T>>>> stack;
	Puntero<NodoAVL<T>> raiz;
	Puntero<NodoAVL<T>> auxRaiz;
	
	void apilarHastaElMenor(Puntero<NodoAVL<T>> p){
		while (p != NULL)	{
			stack->Push(p);
			p = p->hIzq;
		}
	}
};

#endif