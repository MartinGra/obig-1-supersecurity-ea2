#ifndef FUNCIONHASHASOCIACION_H
#define FUNCIONHASHASOCIACION_H
#include "Puntero.h"
#include "Asociacion.h"
#include "CadenaFuncionHash.h"

template<class D, class R>
class FuncionHashAsociacion : public FuncionHash <Asociacion<D, R>> {
public:
	FuncionHashAsociacion(Puntero<FuncionHash<D>> p){
		pf = p;
	}
	nat CodigoDeHash(const Asociacion<D, R> & a) const override{
		return pf->CodigoDeHash(a.GetDominio());
	}
	~FuncionHashAsociacion(){
		pf = NULL;
	}
private:
	Puntero<FuncionHash<D>> pf;
};

#endif