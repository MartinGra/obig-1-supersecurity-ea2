#ifndef COMPARADORASCENDENTE_H
#define COMPARADORASCENDENTE_H
template <class T>
class ComparadorAscendente : public Comparador<T> {
public:
	CompRetorno Comparar(const T& t1, const T& t2) const{
		if (t1 < t2) return MENOR; // descendente : if(t1 > t2) return MENOR
		if (t1 > t2) return MAYOR; // if(t1 < t2) return MAYOR
		if (t1 == t2) return IGUALES;
		return DISTINTOS;
	}
};
#endif