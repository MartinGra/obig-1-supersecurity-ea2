#ifndef AVL_H
#define AVL_H
#include "Puntero.h"
#include "NodoAVL.h"
#include "Iterable.h"
#include "IteradorAVL.h"

template <class T>
class AVL : public Iterable<T>{
public:
	Iterador<T> ObtenerIterador() const override;
	AVL();
	~AVL();
	void Vacio();
	void Insertar(const T& x);
	void Borrar(const T& x);
	const T Raiz() const;
	const T& Maximo() const;
	const T& Minimo() const;
	bool esVacio();
	Puntero<NodoAVL<T>> obRaiz(){ return root; }
	bool mismosNodos(Puntero<NodoAVL<T>> &t) const;
	bool pertenece(T x) const;
	bool perteneceRec(Puntero<NodoAVL<T>> t, T x) const;
	T& obtener(T x) const;
	T& obtenerRec(Puntero<NodoAVL<T>> t, T x) const;

	AVL(const AVL& av){
		vario = av.vario;
		cantNodos = av.cantNodos;
		cmp = av.cmp;
		Iterador<T> it = av.ObtenerIterador();
		root = new NodoAVL<T>();
		while (it.HayElemento()){
			Insertar(it.ElementoActual());
			it.Avanzar();
		}
	}
	AVL& operator=(const AVL& av) {
		vario = av.vario;
		cantNodos = av.cantNodos;
		cmp = av.cmp;
		Iterador<T> it = av.ObtenerIterador();
		root = new NodoAVL<T>();
		while (it.HayElemento()){
			Insertar(it.ElementoActual());
			it.Avanzar();
		}	
		return *this;
	};

	int altura(Puntero<NodoAVL<T>> raiz);
	bool esAVLRec(Puntero<NodoAVL<T>> raiz);
	bool esAVL();
	int getCantNodos(){return cantNodos;}

private:
	//Atributos
	Puntero<NodoAVL<T>> root;
	Puntero<Comparador<T>> cmp;
	bool vario;
	int cantNodos;
	//Metodos
	const Puntero<NodoAVL<T>> MaximoImp(Puntero<NodoAVL<T>> raiz) const;
	const T& MinimoImp(Puntero<NodoAVL<T>> raiz) const;
	bool estaBalanceado(Puntero<NodoAVL<T>> raiz);
	bool estaOrdenado(Puntero<NodoAVL<T>> raiz);
	void BorrarImp(Puntero<NodoAVL<T>> &pa, const T&x);
	void BalanceoDBorrar(Puntero<NodoAVL<T>> &pa);
	void BalanceoIBorrar(Puntero<NodoAVL<T>> &pa);
	void InsertarImp(Puntero<NodoAVL<T>> &pa, const T&e);
	void BalanceoIzq(Puntero<NodoAVL<T>> &p);
	void BalanceoDer(Puntero<NodoAVL<T>> &p);
	bool esMayor(T d, Puntero<NodoAVL<T>> raiz);
	bool esMenor(T d, Puntero <NodoAVL<T>> raiz);
	int alturaMin(Puntero<NodoAVL<T>> r);
};

#include "AVL.cpp"

#endif