#ifndef BARRIO_H
#define BARRIO_H
#include "IBarrio.h"
#include "ComparadorClientePorDireccion.h"
#include "Comparador.h"
#include "Lista.h"
#include "ColaPrioridad.h"
#include "CPImp.h"
#include "AVL.h"
#include "NodoAVL.h"
#include "Cliente.h"
class Barrio:public IBarrio{

private:
	Cadena nombre;
	nat cantCamaras;
	nat promedioHurtos;
	Puntero<AVL<Puntero<Cliente>>> clientes;

public:

	Barrio(Cadena nom, nat cantC, nat promH){
		nombre = nom;
		cantCamaras = cantC;
		promedioHurtos = promH;
		clientes = new AVL<Puntero<Cliente>>();
	}
	Barrio(Cadena nom){
		nombre = nom;
		cantCamaras = 0;
		promedioHurtos = 0;
		clientes = new AVL<Puntero<Cliente>>();
	}
	Barrio(){
		nombre = "";
		cantCamaras = 0;
		promedioHurtos = 0;
		clientes = NULL;
	}
	void setNombre(Cadena s){
		nombre = s;
	}

	~Barrio(){
		clientes = NULL;
	}

	Cadena ObtenerNombreBarrio() const override{
		return nombre;
	};
	Puntero<AVL<Puntero<Cliente>>> ObtenerClientes() const{
		return clientes;
	};
	nat ObtenercantidadCamaras() const override{
		return cantCamaras;
	};
	nat ObtenerpromedioHurtos() const override{
		return promedioHurtos;
	};
	Iterador<pCliente> ObtenerClientesPorDireccion() const override{
		Puntero<Comparacion<Puntero<Cliente>>> compClienteDir = new ComparadorClientePorDireccion();
		Puntero<Comparador<Puntero<Cliente>>> pCompCliente = new Comparador<Puntero<Cliente>>(compClienteDir);
		Puntero<Iterador<Puntero<Cliente>>> it = new Iterador<Puntero<Cliente>>(clientes->ObtenerIterador());
		int largo = 0;
		while (it->HayElemento()){ //O(n)
			largo++;
			it->Avanzar();
		}
		it->Reiniciar();
		Puntero<CP<Puntero<Cliente>, Puntero<Cliente>>> colaDirecciones = new CPImp<Puntero<Cliente>, Puntero<Cliente>>(largo,pCompCliente);
		while (it->HayElemento()){ //O(n)
			Puntero<Cliente> cliTemp = it->ElementoActual();
			colaDirecciones->Encolar(cliTemp, cliTemp);// O(log n)
			it->Avanzar();
		}//O(n * log n)

		Puntero<Lista<Puntero<Cliente>>> lst = new Lista<Puntero<Cliente>>();
		while (!colaDirecciones->EsVacia()){
			lst->Insertar(colaDirecciones->Frente());//O(1) 
			colaDirecciones->Desencolar(); //O(log n)
		}
		return lst->ObtenerIterador();
	}
	bool operator==(const IBarrio& b) const override{
		return ObtenerNombreBarrio() == b.ObtenerNombreBarrio();
	}
	
	bool operator==(const Barrio& b) const {
		return ObtenerNombreBarrio() == b.ObtenerNombreBarrio();
	}

	bool operator!=(const Barrio& c) const{
		return !(c == *this);
	}
	bool operator<(const Barrio& c) const{ 
		return nombre < c.ObtenerNombreBarrio();
	}
	bool operator>(const Barrio& c) const{ 
		return nombre > c.ObtenerNombreBarrio() ;
	}
	bool operator<=(const Barrio& c) const{
		return ObtenerNombreBarrio() <= c.ObtenerNombreBarrio();
	}
	bool operator>=(const Barrio& c) const{
		return ObtenerNombreBarrio() >= c.ObtenerNombreBarrio();
	}

	friend ostream& operator<<(ostream& out, const Barrio& b){
		out << b.nombre;
		return out;
	}
	Barrio(const Barrio& b) {
		cantCamaras = b.cantCamaras;
		promedioHurtos = b.promedioHurtos;
		nombre = b.nombre;
		clientes = b.clientes;
	}
	Barrio& operator=(const Barrio& b) {
		if (this != &b){
			nombre = b.nombre;
			cantCamaras = b.cantCamaras;
			promedioHurtos = b.promedioHurtos;
			clientes = b.clientes;
		}
		return *this; 
	}

};


#endif