#ifndef AVL_CPP
#define AVL_CPP
#include "AVL.h"
#include <assert.h>
#include <cmath>

template <class T>
Iterador<T> AVL<T>::ObtenerIterador() const{
	return new IteradorAVL<T>(root);
}
template<class T>
bool AVL<T>::mismosNodos(Puntero<NodoAVL<T>> &t) const{
	if (!root && !t) return true;
	Iterador<T> iter = ObtenerIterador();
	while (iter.HayElemento()){
		if (!pertenece(t, iter.ElementoActual())) return false;
		iter.Avanzar();
	}
	return true;
}
template<class T>
bool AVL<T>::pertenece(T x) const{
	return perteneceRec(root, x);
}

template<class T>
bool AVL<T>::perteneceRec(Puntero<NodoAVL<T>> t, T x) const{
	if (!t) return false;
	else {
		if (t->dato == x) return true;
		if (t->dato > x) return perteneceRec(t->hIzq, x);
		else return perteneceRec(t->hDer, x);
	}
}
template<class T>
T& AVL<T>::obtener(T x) const{
	return obtenerRec(root, x);
}

template<class T>
T& AVL<T>::obtenerRec(Puntero<NodoAVL<T>> t, T x) const{
	if (t->dato == x) return t->dato;
	if (t->dato > x) return obtenerRec(t->hIzq, x);
	else return obtenerRec(t->hDer, x);
	
}
template <class T>
 AVL<T>::AVL(){
    this->root = NULL;
	this->cmp = NULL;
	this->vario = false;
	this->cantNodos = 0;
}
template <class T>
AVL<T>::~AVL(){
	Vacio();
}
template <class T>
void AVL<T>:: Vacio(){
	this->root = NULL;
	this->cmp = NULL;
	this->cantNodos = 0;
	this->vario = false;
}
template <class T>
void AVL<T>::Insertar(const T& e){
	InsertarImp((this->root), e);
	cantNodos++;
}

template <class T>
void AVL<T>::Borrar(const T& x){
	BorrarImp(this->root, x);
}

template <class T>
void AVL<T>::BorrarImp(Puntero<NodoAVL<T>> &pa, const T&e){
	if (pa){
		if (e > pa->dato){
			BorrarImp(pa->hDer, e);
			if (!estaBalanceado(root)){ // no sabemos si es balanceo de pa
				BalanceoDBorrar(root);
			}
		}
		else if (e < pa->dato){
			BorrarImp(pa->hIzq, e);
			if (!estaBalanceado(root)){ //no sabemos si es balanceo de pa
				BalanceoIBorrar(root);
			}
		}
		else {
			if (pa->hDer && pa->hIzq){
				Puntero<NodoAVL<T>> maxIzq = MaximoImp(pa->hIzq);
				pa->dato = maxIzq->dato;
				BorrarImp(pa->hIzq, maxIzq->dato);
			}
			else{
				Puntero<NodoAVL<T>> temp = pa;
				if (pa->hDer){
					pa = pa->hDer;
					temp = NULL;
				}
				else if (pa->hIzq){
					pa = pa->hIzq;
					temp = NULL;
				}
				else{
					pa = NULL;
					temp = NULL;
				}
				cantNodos--;
			}
		}
	}
}

template <class T>
void AVL<T>::BalanceoDBorrar(Puntero<NodoAVL<T>> &pa){
	if (pa){
		Puntero<NodoAVL<T>> p1 = new NodoAVL < T >();
		Puntero<NodoAVL<T>> p2 = new NodoAVL < T >();
		int b1, b2;
		switch (pa->fb){
		case 1:
			pa->fb = 0;
			break;
		case 0:
			pa->fb = -1;
			this->vario = false;
			break;
		case -1:
			p1 = pa->hIzq;
			b1 = p1->fb;
			if (b1 <= 0){
				pa->hIzq = pa->hDer;
				p1->hDer = pa;
				if (b1 == 0){
					pa->fb = -1;
					p1->fb = 1;
					this->vario = false;
				}
				else pa->fb = p1->fb = 0;
				pa = p1;
			}
			else{
				p2 = p1->hDer;
				b2 = p2->fb;
				p1->hDer = p2->hIzq;
				p2->hIzq = p1;
				pa->hIzq = p2->hDer;
				p2->hDer = pa;
				pa->fb = b2 == -1 ? 1 : 0;
				p1->fb = b2 == 1 ? -1 : 0;
				pa = p2;
				p2->fb = 0;
			}
		}
	}
}

template <class T>
void AVL<T>::BalanceoIBorrar(Puntero<NodoAVL<T>> & pa){
	if (pa){
	Puntero<NodoAVL<T>> p1 = new NodoAVL<T>();
	Puntero<NodoAVL<T>> p2 = new NodoAVL<T>();
	int d1, d2;
	switch (pa->fb){
	case -1:
		pa->fb = 0;
		break;
	case 0:
		pa->fb = 1;
		this->vario = false;
		break;
	case 1:
		p1 = pa->hDer;
		d1 = p1->fb;
		if (d1 >= 0){
			pa->hDer = p1->hIzq;
			p1->hIzq = pa;
			if (d1 == 0){
				pa->fb = 1;
				p1->fb = -1;
				this->vario = false;
			}
			else pa->fb = p1->fb = 0;
			pa = p1;
		}
		else{
			p2 = p1->hIzq;
			d2 = p2->fb;
			p1->hIzq = p2->hDer;
			p2->hDer = p1;
			pa->hDer = p2->hIzq;
			p2->hIzq = pa;
			pa->fb = d2 == 1 ? -1 : 0;
			p1->fb = d2 == -1 ? 1 : 0;
			pa = p2;
			p2->fb = 0;
		}
	}
	}
}
template <class T>
void AVL<T> ::InsertarImp(Puntero<NodoAVL<T>> &pa, const T&e){
	if (pa == NULL){ //Se da el alta
		pa = new NodoAVL<T>(e);
		pa->hDer = pa->hIzq = NULL;
		this->vario = true;
	}
	
	else if (pa->dato > e){
		InsertarImp(pa->hIzq, e);
		if (this->vario) BalanceoIzq(pa); 
	}
	else if (pa->dato < e){
		InsertarImp(pa->hDer, e);
		if (this->vario) BalanceoDer(pa);
	}
	else this->vario = false; // Ya esta en el arbol
	
}

template <class T>
void AVL<T>::BalanceoIzq(Puntero<NodoAVL<T>> &p){
	Puntero<NodoAVL<T>> p1 = new NodoAVL <T>();
	Puntero<NodoAVL<T>> p2 = new NodoAVL <T>();
	
	switch (p->fb){
	case 1:
		p->fb = 0;
		this->vario = false;
		break;
	case 0:
		p->fb = -1;
		break;
	case -1:
		p1 = p->hIzq;
		if (p1->fb == -1){ // II
			p->hIzq = p1->hDer;
			p1->hDer = p;
			p->fb = 0;
			p = p1;
		}
		else{ // ID
			p2 = p1->hDer;
			p1->hDer = p2->hIzq;
			p2->hIzq = p1;
			p->hIzq = p2->hDer;
			p2->hDer = p;
			p->fb = p2->fb == -1 ? 1 : 0;
			p1->fb = p2->fb == 1 ? -1 : 0;
			p = p2;
		}
		p->fb = 0;
		this->vario = false;
	}
}

template <class T>
void AVL<T>::BalanceoDer(Puntero<NodoAVL<T>> &p){
	Puntero<NodoAVL<T>> p1 = new NodoAVL <T>();
	Puntero<NodoAVL<T>> p2 = new NodoAVL <T>();
	switch (p->fb){
	case -1:
		p->fb = 0;
		this->vario = false;
		break;
	case 0:
		p->fb = 1;
		break;
	case 1:
		p1 = p->hDer;
		if (p1->fb == 1){ // DD
			p->hDer = p1->hIzq;
			p1->hIzq = p;
			p->fb = 0;
			p = p1;
		}
		else{ //DI
			p2 = p1->hIzq;
			p1->hIzq = p2->hDer;
			p2->hDer = p1;
			p->hDer = p2->hIzq;
			p2->hIzq = p;
			p->fb = p2->fb == 1 ? -1 : 0;
			p1->fb = p2->fb == -1 ? 1 : 0;
			p = p2;
		}
		p->fb = 0;
		this->vario = false;
	}
}

template <class T>
const T  AVL<T>::Raiz() const{
	if (!root) return NULL;
	return root->dato;
}
template <class T>
const T& AVL<T>::Maximo() const{
	return MaximoImp(this->root)->dato;
}
template<class T>
const Puntero<NodoAVL<T>> AVL<T>::MaximoImp(Puntero<NodoAVL<T>> p) const{
	if (!p->hDer) return p;
	else return MaximoImp(p->hDer);
}
template <class T>
const T& AVL <T>::Minimo() const{
	return MinimoImp(this->root);
}
template <class T>
const T& AVL <T>::MinimoImp(Puntero<NodoAVL<T>> p) const{
	assert(p != NULL);
	if (!p->hIzq) return p->dato;
	else return MinimoImp(p->hIzq);
}	
template <class T>
bool AVL<T>::esAVLRec(Puntero<NodoAVL<T>> r){
	return estaBalanceado(r) && estaOrdenado(r);
}
template <class T>
bool AVL<T>::esAVL(){
	return esAVLRec(root);
}
template <class T>
bool AVL<T>::estaBalanceado(Puntero<NodoAVL<T>> r){
	if (!r) return true;
	else{
		return abs(altura(r->hDer) - altura(r->hIzq)) <2 &&
			estaBalanceado(r->hIzq) && estaBalanceado(r->hDer);
	}
}
template<class T>
int AVL<T> ::altura(Puntero<NodoAVL<T>> r){
	if (!r) return 0;
	else{
		return 1 + fmax(altura(r->hDer), altura(r->hIzq));
	}
}

template<class T>
int AVL<T>::alturaMin(Puntero<NodoAVL<T>> r){
	if (!r) return 0;
	else{
		return 1 - fmin(altura(r->hDer), altura(r->hIzq));
	}
}
template <class T>
bool AVL<T>::estaOrdenado(Puntero<NodoAVL<T>> r){
	if (!r) return true;
	else{
		return esMayor(r->dato, r->hIzq) && esMenor(r->dato, r->hDer)
			&& estaOrdenado(r->hIzq) && estaOrdenado(r->hDer);
	}
}
template <class T>
bool AVL<T>::esVacio(){
	return this->root == NULL;
}

template<class T>
bool AVL<T>::esMayor(T d, Puntero<NodoAVL<T>> r){
	if (!r) return true;
	else return d > r->dato;
}
template<class T>
bool AVL<T>::esMenor(T d, Puntero<NodoAVL<T>> r){
	if (!r) return true;
	else return d < r->dato;
}
#endif