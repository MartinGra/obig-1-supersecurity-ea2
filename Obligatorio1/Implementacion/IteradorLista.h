#ifndef ITERADORLISTA_H
#define ITERADORLISTA_H
#include "Iterador.h"
#include "Iteracion.h"
#include "Lista.h"
#include "Puntero.h"
#include "NodoLista.h"
template<class T>
class IteradorLista : public Iteracion<T>{
public:

	IteradorLista(Puntero<NodoLista<T>> unNodo){
		comienzo = unNodo;
		aux = unNodo;
	}
	bool HayElemento() const {
		return comienzo != NULL;
	}
	const T& ElementoActual() const {
		return comienzo->obj;
	}
	void Avanzar(){
		comienzo = comienzo->sig;
	}
	void Reiniciar(){
		comienzo = NULL;
		comienzo = aux;
	}
private:
	Puntero<NodoLista<T>> comienzo;
	Puntero<NodoLista<T>> aux;
};

#endif