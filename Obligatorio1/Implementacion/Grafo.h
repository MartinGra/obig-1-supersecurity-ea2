#ifndef GRAFO_H
#define GRAFO_H
#include "Puntero.h"
#include "Iterador.h"

template<class V, class A>
//Soluciona problemas planteados con multigrafos. Evaluar si poner o no para inhabilitar aristas/vertices en vez de eliminarlas o darlas de baja
class Grafo{
public:
	virtual ~Grafo(){};
	//PRE: - No llega a maxVertices y el vertice a agregar no pertenece al grafo
	virtual void agregarVertice(V) abstract;
	//POS: Agrega el vertice al grafo

	//PRE: -
	virtual void agregarArista(A,V,V) abstract;
	//POS: Agrega la arista al grafo entre los dos vertice

	//PRE: El vertice pertenece al grafo
	//virtual void inhabilitarVertice(V) abstract;
	//POS: Asegura la inhabilitacion del vertice del grafo

	//PRE: La arista pertenece al grafo
	//virtual void inhabilitarArista(A) abstract;
	//POS: Asegura la inhabilitacion de la Arista del grafo
	//PRE: -
	virtual int getCantVertices() abstract;
	//POS: Retorna la cantidad de vertices que posee el grafo

	//PRE: -
	virtual int getMaxVertices() abstract;
	//POS: Retorna la cantidad maxima de vertices que puede tener el grafo

	//PRE: -
	virtual int getCantAristas() abstract;
	//POS: Retorna la cantidad de aristas que posee el grafo

	virtual bool existeVertice(int i) const abstract;
	//PRE: -
	virtual V getVertice(int i) abstract;
	//POS: Retorna el vertice que le corresponde dicho indice, NULL en caso de no pertenecer al grafo

	//PRE: -
	virtual int getVertice(V) abstract;
	//POS: Retorna el vertice que le corresponde dicho indice, NULL en caso de no pertenecer al grafo

	//PRE: El origen y destino pertenecen al grafo
	virtual Iterador<A> aristas(V origen, V destino) abstract;
	//POS: Devuelve un iterador sobre las aristas que conectan al origen y al destino

	//PRE: El vertice pertenece al grafo
	virtual Iterador<V> adyacentes(V) abstract;
	//POS: Devuelve un iterador sobre los vertices adyacentes al vertice parametro
	virtual V getOrigen(A) abstract;
	virtual V getDestino(A) abstract;
	virtual Puntero<Grafo<V,A>> Clon() const abstract;
	virtual Iterador<A> getTodasAristas() abstract;
	virtual Matriz<Puntero<Lista<A>>> getMatrizAdy() abstract;
};

#endif