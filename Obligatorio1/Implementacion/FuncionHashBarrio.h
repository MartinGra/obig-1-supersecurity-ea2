#ifndef FUNCIONHASHBARRIO_H
#define FUNCIONHASHBARRIO_H

#include "Puntero.h"
#include "Asociacion.h"
#include "CadenaFuncionHash.h"

class FuncionHashBarrio : public FuncionHash <Puntero<Barrio>> {
public:
	FuncionHashBarrio(Puntero<FuncionHash<Cadena>> p){
		pf = p;
	}
	nat CodigoDeHash(const Puntero<Barrio> & a) const override{
		return pf->CodigoDeHash(a->ObtenerNombreBarrio());
	}

	~FuncionHashBarrio(){
		pf = NULL;
	}
private:
	Puntero<FuncionHash<Cadena>> pf;
};

#endif