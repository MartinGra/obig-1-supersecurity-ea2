﻿#include "CasoDePrueba.h"
#include "PruebaMemoria.h"
#include "ConductorPrueba.h"
#include "Sistema.h"
#include "Puntero.h"
#include <string>
#include "AVL.h"
#include "NodoAVL.h"
#include "Lista.h"
#include "PilaImp.h"
#include "Iterador.h"
#include "CPImp.h"
#include "ColaPrioridad.h"
#include "TablaImpHash.h"
#include "FuncionHash.h"
#include <cstdlib>
#include <iomanip>
#include "Hash.h"
#include "Asociacion.h"
#include "FuncionHashAsociacion.h"
#include "Grafo.h"
#include "CadenaFuncionHash.h"
#include "FuncionHashBarrio.h"
#include "grafoImpMatriz.h"
#include "Vertice.h"
#include "ComparadorBarrioCantClientes.h"
#include "FuncionHashVertice.h"
#include "Comparador.h"
#include "Comparacion.h"
#include "Recorrido.h"

Puntero<ISistema> Inicializar(nat MAX_BARRIOS, nat MAX_CLIENTES)
{
	return new Sistema(MAX_BARRIOS, MAX_CLIENTES);
}

template<class T>
void ImprNom(Puntero<NodoAVL<T>>a, int indent){
	if (a != NULL){
		ImprNom(a->hDer, indent + 3);
		if (indent > 0)
			cout << setw(indent) << " ";
		cout << a->dato << endl;
		ImprNom(a->hIzq, indent + 3);
	}
}

void ImprCP(Puntero<CP<Puntero<Barrio>,Puntero<Barrio>>> cp, int indent, int index, int tope){
	if (index <= tope){
		ImprCP(cp, indent + 3, 2 * index + 1, tope);
		if (indent > 0)
			cout << setw(indent) << " ";
		cout <<cp->getHeap()[index] << endl;
		ImprCP(cp, indent + 3, 2 * index, tope);
	}
}

void main()
{
	clock_t startTime = clock();
	
	//Pruebas Sistema
	Puntero<ConductorPrueba> cp = new ConductorPrueba();

	Array<Puntero<Prueba>> pruebas = Array<Puntero<Prueba>>(3);

	pruebas[0] = new PruebaMemoria();

	pruebas[1] = new CasoDePrueba(Inicializar);

	pruebas[2] = pruebas[0];

	cp->CorrerPruebas(pruebas.ObtenerIterador());

	cout << double(clock() - startTime) / (double)CLOCKS_PER_SEC << " seconds." << endl;
	int dato; cin >> dato;
	
}
