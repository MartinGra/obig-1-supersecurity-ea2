#ifndef FUNCIONHASHVERTICE_H
#define FUNCIONHASHVERTICE_H

#include "Puntero.h"
#include "Asociacion.h"
#include "CadenaFuncionHash.h"

class FuncionHashVertice : public FuncionHash <Puntero<Vertice<Barrio>>> {
public:
	FuncionHashVertice(Puntero<FuncionHash<Cadena>> p){
		pf = p;
	}
	nat CodigoDeHash(const Puntero<Vertice<Barrio>> & v) const override{
		return pf->CodigoDeHash(v->getVertice()->ObtenerNombreBarrio());
	}
	~FuncionHashVertice(){
		pf = NULL;
	}
private:

	Puntero<FuncionHash<Cadena>> pf;
};

#endif