#ifndef tabla_h
#define tabla_h
#include "Puntero.h"
template<class D, class R>
class Tabla abstract{
public:
	//PRE: no esta llena la tabla
	virtual void Agregar(const D&, const R&)abstract;
	//POS: en la tabla quedan asociados D y R donde Recuperar(d) := r

	virtual ~Tabla(){};

	//PRE: none
	virtual bool Eliminar(const D&)abstract;
	//POS: !estadoDefinido(d)

	//PRE: estaDef(d)
	virtual R Recuperar(const D&)const abstract;
	//POS: retorna el valor asociado a d (es decir, r que es de tipo R)

	//PRE: none
	virtual bool EstaDef(const D&) const abstract;
	//POS: retorna true solamente si D esta definido en la tabla con un valor asociado

	//PRE: none
	//virtual bool Relacion(const D&, const R&);
	//POS: nos indica si D esta relacionado con r **NO ESTAMOS SEGUROS

	//PRE: none
	//virtual R&operator[](const D&) abstract; No lo utilizamos en este proyecto
	// Devuelve el R asociado a [d] y en caso de no existir lo crea con el constructor sin parametros

	//PRE: 
	virtual Puntero<Tabla<D, R>> Clon() const abstract;
	//POS: retorna una copia de this que no comparte memoria

	//PRE: 
	virtual Puntero<Tabla<D, R>> TablaVacia() const abstract;
	//POS: retorna una tabla vacia

	//PRE:
	virtual void Vaciar() abstract;
	//POS: el metodo esVacia() retorna true

	//PRE:
	virtual bool esVacia() const abstract;
	//POS: retorna true si la tabla no tiene elementos

	//PRE:
	//virtual Iterador<D> claves() const abstract;
	//POS: retorna un iterador sobrel os datos de los elementos del dominio
	virtual void imprimirTab() abstract;
};

#endif