﻿#ifndef SISTEMA_H
#define SISTEMA_H

#include "ISistema.h"
#include "AVL.h"
#include "Puntero.h"
#include "Cliente.h"
#include "Barrio.h"
#include "TablaImpHash.h"
#include "Barrio.h"
#include "GrafoImpMatriz.h"
#include "NaturalFuncionHash.h"
#include "AristaConexion.h"
#include "FuncionHashBarrio.h"
#include "FuncionHashVertice.h"
#include "Vertice.h"
#include "AristaConexion.h"
#include "Recorrido.h"
#include "ComparadorBarrioCantClientes.h"
#include "ComparadorClientePorCI.h"
#include "ColaPrioridad.h"
#include "Grafo.h"
#include "CPImp.h"
#include <climits>
class Sistema : public ISistema
{
public:
	Sistema(nat MAX_BARRIOS, nat MAX_CLIENTES);

	// Tipo 1
	TipoRetorno IngresoBarrio(Cadena nombreBarrio, nat cantidadCamaras, nat promedioHurtos) override;
	TipoRetorno IngresoCliente(nat ciCliente, Cadena nombreCliente, nat fechaRegCliente, Cadena nombreBarrio, Cadena direccion) override;
	Tupla<TipoRetorno, pCliente> ConsultaCliente(nat ciCliente) override;
	Tupla<TipoRetorno, Iterador<pBarrio>> ListadoBarrios() override;
	Tupla<TipoRetorno, Iterador<pCliente>> ListadoClientes() override;
	TipoRetorno IngresoConexion(Cadena nombreBarrioOrigen, Cadena nombreBarrioDestino, nat distancia, nat tiempo) override;
	Tupla<TipoRetorno, pRecorrido> CaminoMasRapido(Cadena nombreClienteOrigen, Cadena nombreClienteDestino) override;

	// Tipo 2
	TipoRetorno BajaCliente(nat ciCliente) override;
	Tupla<TipoRetorno, pBarrio> ConsultaBarrio(Cadena nombreBarrio) override;
	Tupla<TipoRetorno, pBarrio> BarrioConMasClientes() override;
	Tupla<TipoRetorno, pRecorrido> CaminoMenorDistancia(Cadena nombreBarrioOrigen, Cadena nombreBarrioDestino) override;
	Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> CableadoMinimo() override;

	Puntero<AVL<Puntero<Barrio>>> getBarrios(){return avlBarrios;}
	Puntero <AVL<Puntero<Cliente>>> getClientes(){return avlClientes;}
	Puntero<Tabla<nat, Puntero<Cliente>>> obtenerHashClientes(){return hashClientes;}
	Puntero<GrafoImpMatriz<Puntero<Vertice<Barrio>>, AristaConexion>> obtenerGrafoBarrios(){return grafoBarrios;}

	Puntero<AristaConexion> obtenerMinimaArista(Puntero<Lista<AristaConexion>> l, bool priorizaTiempo);
	Matriz<Puntero<AristaConexion>>matrizAristasMenores(Matriz<Puntero<Lista<AristaConexion>>> mat,bool);
	Matriz<Puntero<Lista<Puntero<AristaConexion>>>> floydLista(Matriz<Puntero<AristaConexion>> path,bool);
	Tupla<nat, nat, Array<Array<int>>> kruskal(Matriz<Puntero<AristaConexion>>);

private:
	Puntero<AVL<Puntero<Cliente>>> avlClientes;
	Puntero<AVL<Puntero<Barrio>>> avlBarrios;
	Puntero<Tabla<nat, Puntero<Cliente>>> hashClientes;
	nat MAX_BARRIOS;
	nat MAX_CLIENTES;
	Puntero<Grafo<Puntero<Vertice<Barrio>>, AristaConexion>> grafoBarrios;
	Puntero<CP<Puntero<Barrio>, Puntero<Barrio>>> cpBarriosPorClientes;
};

#endif
