﻿#ifndef PILAIMP_H
#define PILAIMP_H
#include "Puntero.h"
#include "Lista.h"
#include "Iteracion.h"
template <class T>
class PilaImp : public Iteracion<T>{
public:
	~PilaImp(){
		stack = NULL;
	}
	PilaImp();
	void Push(const T& e);
	const T& Top() const;
	void Pop();
	void Vaciar();
	bool esVacia() const;
	const T& ElementoActual() const override;
	void Avanzar() override;
	void Reiniciar() override;
	bool HayElemento() const override;

private:
	Puntero <NodoLista<T>> stack;
};

#include "PilaImp.cpp"

#endif