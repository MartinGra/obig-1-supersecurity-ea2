#ifndef GRAFOIMPMATRIZ_H
#define GRAFOIMPMATRIZ_H
#include "Tabla.h"
#include "Matriz.h"
#include "Lista.h"
#include "Grafo.h"
#include "Puntero.h"
#include "TablaImpHash.h"
#include "FuncionHashAsociacion.h"
template <class V, class A>
class GrafoImpMatriz : public Grafo<V,A>{
private:
	nat maxVertices;
	nat cantVertices;
	nat cantAristas;
	Puntero<TablaImpHash<V, nat>> tablaVaNat;
	Puntero<TablaImpHash<nat,V>> tablaNataV;
	Matriz<Puntero<Lista<A>>> matrizAdy;

public:
	GrafoImpMatriz(nat topeVertices, Puntero<FuncionHashAsociacion<V, nat>> pfhVN, Puntero<FuncionHashAsociacion<nat,V>> pfhNV){
		tablaVaNat = new TablaImpHash<V, nat>(topeVertices, pfhVN);
		tablaNataV = new TablaImpHash<nat, V>(topeVertices, pfhNV);
		maxVertices = tablaVaNat->getHash()->getArray().ObtenerLargo(); 
		matrizAdy = Matriz<Puntero<Lista<A>>>(maxVertices); 
		cantVertices = 0;
		cantAristas = 0;
	}
	~GrafoImpMatriz(){
		tablaNataV = NULL;
		tablaNataV = NULL;
		matrizAdy = NULL;
	}
	
	void agregarVertice(V)override; 
	void agregarArista(A,V,V) override; //PRE: Los vertices pertenecen previamente al grafo
	int getCantVertices() override;
	int getMaxVertices() override;
	int getCantAristas() override;
	V getVertice(int i) override;
	int getVertice(V) override;

    Iterador<A> aristas(V origen, V destino) override;
	Iterador<V> adyacentes(V) override;
	bool existeVertice(int posV) const override;
	V getOrigen(A) override;
	V getDestino(A) override;
	Puntero<Grafo<V,A>> Clon() const override;
	Iterador<A> getTodasAristas() override;

	Matriz<Puntero<Lista<A>>> getMatrizAdy() override{
		return matrizAdy;
	}
	
};
#include "grafoImpMatriz.cpp"

#endif 