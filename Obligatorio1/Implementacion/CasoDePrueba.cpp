﻿#include "CasoDePrueba.h"
#include "PruebasMock.h"

CasoDePrueba::CasoDePrueba(Puntero<ISistema> (*inicializar)(nat MAX_BARRIOS, nat MAX_CLIENTES))
{
	this->inicializar = inicializar;
}

Puntero<ISistema> CasoDePrueba::InicializarSistema(nat MAX_BARRIOS, nat MAX_CLIENTES)
{
	Puntero<ISistema> interfaz = inicializar(MAX_BARRIOS, MAX_CLIENTES);
	ignorarOK = false;
	return interfaz;
}

Array<pCliente> CasoDePrueba::InicializarCliente(Puntero<ISistema> interfaz, bool ignOk)
{
	ignorarOK = ignOk;

	Array<pCliente> clientes(15);
	// Desordenados
	clientes[0] = new ClienteMock(470, "Pedro", 20150904, "Centro", "Andes 1417");
	clientes[1] = new ClienteMock(47, "Juan", 20150904, "Centro", "Cuareim 1517");	
	clientes[2] = new ClienteMock(43749284, "Santiago", 20150523, "Buceo", "L A de Herrera 183");
	clientes[3] = new ClienteMock(21341, "Lucía", 20150523, "Prado", "Burgues 3921");
	clientes[4] = new ClienteMock(333, "Jose", 20150520, "Prado", "Millan 4532");
	clientes[5] = new ClienteMock(4723, "Federica", 20150904, "Buceo", "26 de Marzo 171");
	clientes[6] = new ClienteMock(5, "Edgardo", 20150523, "Prado", "Joaquin Suarez 1234");
	clientes[7] = new ClienteMock(46823, "Zoraida", 20150521, "3 Ombues", "Rivera 912");
	clientes[8] = new ClienteMock(47001281, "Horacio", 20150904, "Cordon", "Sarmiento 1121");
	clientes[9] = new ClienteMock(123432, "Tomas", 20150523, "Cordon", "Juan Manuel Blanes 1120");
	clientes[10] = new ClienteMock(47112, "Maria", 20150904, "Pocitos", "Obligado 157");
	clientes[11] = new ClienteMock(44444, "Martín", 20140521, "Prado", "L A de Herrera 7112");
	clientes[12] = new ClienteMock(32789, "Umberto", 20121129, "Centro", "18 de Julio 3212");
	clientes[13] = new ClienteMock(4921, "Cristina", 20111211, "Cordon", "Constituyente 9812");
	clientes[14] = new ClienteMock(16321, "Alberto", 20080101, "Cordon", "Jackson 7112");

	Cadena texto = "Se ingresa el Cliente '{0}'.";

	for(int i = clientes.Largo -1; i >= 0; i --){
		Verificar(interfaz ->IngresoCliente(clientes[i]->ObtenerCiCliente(), clientes[i]->ObtenerNombreCliente(), clientes[i]->ObtenerFechaRegCliente(), clientes[i]->ObtenerNombreBarrio(), clientes[i]->ObtenerDireccion()), OK, texto.DarFormato(clientes[i]->ObtenerNombreCliente()));
	}

	ignorarOK = false;

	return clientes;
}


Array<pCliente> CasoDePrueba::InicializarCliente2(Puntero<ISistema> interfaz, bool ignOk)
{
	ignorarOK = ignOk;

	Array<pCliente> clientes(15);
	// Orden creciente por nombre
	clientes[0] = new ClienteMock(16321, "Alberto", 20080101, "Cordon", "Jackson 7112");
	clientes[1] = new ClienteMock(4921, "Cristina", 20111211, "Cordon", "Constituyente 9812");
	clientes[2] = new ClienteMock(5, "Edgardo", 20150523, "Prado", "Joaquin Suarez 1234");
	clientes[3] = new ClienteMock(4723, "Federica", 20150904, "Buceo", "26 de Marzo 171");
	clientes[4] = new ClienteMock(47001281, "Horacio", 20150904, "Cordon", "Sarmiento 1121");
	clientes[5] = new ClienteMock(333, "Jose", 20150520, "Prado", "Millan 4532");
	clientes[6] = new ClienteMock(47, "Juan", 20150904, "Centro", "Cuareim 1517");
	clientes[7] = new ClienteMock(1048282, "Lucía", 20150523, "Prado", "Burgues 3921");
	clientes[8] = new ClienteMock(47112, "Maria", 20150904, "Pocitos", "Obligado 157");
	clientes[9] = new ClienteMock(99999999, "Martin", 20140521, "Prado", "L A de Herrera 7112");
	clientes[10] = new ClienteMock(470, "Pedro", 20150904, "Centro", "Andes 1417");
	clientes[11] = new ClienteMock(43749284, "Santiago", 20150523, "Buceo", "L A de Herrera 183");
	clientes[12] = new ClienteMock(123432, "Tomas", 20150523, "Cordon", "Juan Manuel Blanes 1120");
	clientes[13] = new ClienteMock(32789, "Umberto", 20121129, "Centro", "18 de Julio 3212");
	clientes[14] = new ClienteMock(46823, "Zoraida", 20150521, "3 Ombues", "Rivera 912");

	Cadena texto = "Se ingresa el Cliente '{0}'.";

	for(int i = clientes.Largo -1; i >= 0; i --){
		Verificar(interfaz ->IngresoCliente(clientes[i]->ObtenerCiCliente(), clientes[i]->ObtenerNombreCliente(), clientes[i]->ObtenerFechaRegCliente(), clientes[i]->ObtenerNombreBarrio(), clientes[i]->ObtenerDireccion()), OK, texto.DarFormato(clientes[i]->ObtenerNombreCliente()));
	}

	ignorarOK = false;

	return clientes;
}

Array<pBarrio> CasoDePrueba::InicializarBarrio(Puntero<ISistema> interfaz, bool ignOk)
{
	ignorarOK = ignOk;

	Array<pBarrio> barrios(5);
	// Orden creciente por direccion

	barrios[0] = new BarrioMock("Buceo", 10, 20, Iterador<pCliente>());
	barrios[1] = new BarrioMock("Centro", 40, 45, Iterador<pCliente>());
	barrios[2] = new BarrioMock("Cordon", 5, 40, Iterador<pCliente>());
	barrios[3] = new BarrioMock("Pocitos", 20, 10, Iterador<pCliente>());
	barrios[4] = new BarrioMock("Prado", 60, 60, Iterador<pCliente>());

	Cadena texto = "Se ingresa el barrio '{0}'.";


		for(int i = barrios.Largo - 1; i >= 0; i --){
			Verificar(interfaz->IngresoBarrio(barrios[i]->ObtenerNombreBarrio(), barrios[i]->ObtenercantidadCamaras(), barrios[i]->ObtenerpromedioHurtos()), OK, texto.DarFormato(barrios[i]->ObtenerNombreBarrio()));
		}

	interfaz->IngresoCliente(1048282, "Lucía", 20150523, "Prado", "Burgues 3921");
	interfaz->IngresoCliente(5, "Edgardo", 20150523, "Prado", "Joaquin Suarez 1234");
	interfaz->IngresoCliente(99999999, "Martin", 20150521, "Prado", "L A de Herrera 7112");
	interfaz->IngresoCliente(333, "Jose", 20150520, "Prado", "Millan 4532");
	ignorarOK = false;

	return barrios;
}

Array<pBarrio> CasoDePrueba::InicializarBarrio5(Puntero<ISistema> interfaz, bool ignOk) {

	ignorarOK = ignOk;

	Array<pBarrio> barrios(5);
	// Orden creciente por direccion

	barrios[0] = new BarrioMock("Buceo", 10, 20, Iterador<pCliente>());
	barrios[1] = new BarrioMock("Centro", 40, 45, Iterador<pCliente>());
	barrios[2] = new BarrioMock("Cordon", 5, 40, Iterador<pCliente>());
	barrios[3] = new BarrioMock("Pocitos", 20, 10, Iterador<pCliente>());
	barrios[4] = new BarrioMock("Prado", 60, 60, Iterador<pCliente>());

	Cadena texto = "Se ingresa el barrio '{0}'.";


		for(int i = barrios.Largo - 1; i >= 0; i --){
			Verificar(interfaz->IngresoBarrio(barrios[i]->ObtenerNombreBarrio(), barrios[i]->ObtenercantidadCamaras(), barrios[i]->ObtenerpromedioHurtos()), OK, texto.DarFormato(barrios[i]->ObtenerNombreBarrio()));
		}

	ignorarOK = false;

	return barrios;
}


Array<pBarrio> CasoDePrueba::InicializarBarrio2(Puntero<ISistema> interfaz, bool ignOk)
{
	ignorarOK = ignOk;

	Array<pBarrio> barrios(5);
	// Orden creciente por direccion

	barrios[0] = new BarrioMock("Sayago", 18, 20, Iterador<pCliente>());
	barrios[1] = new BarrioMock("3 Ombues", 20, 45, Iterador<pCliente>());
	barrios[2] = new BarrioMock("Reducto", 5, 10, Iterador<pCliente>());
	barrios[3] = new BarrioMock("Aires Puros", 5, 10, Iterador<pCliente>());
	barrios[4] = new BarrioMock("Malvin", 60, 5, Iterador<pCliente>());

	Cadena texto = "Se ingresa el barrio '{0}'.";

		for(int i = barrios.Largo - 1; i >= 0; i --){
			Verificar(interfaz->IngresoBarrio(barrios[i]->ObtenerNombreBarrio(), barrios[i]->ObtenercantidadCamaras(), barrios[i]->ObtenerpromedioHurtos()), OK, texto.DarFormato(barrios[i]->ObtenerNombreBarrio()));
		}


	ignorarOK = false;

	return barrios;
}

Array<pBarrio> CasoDePrueba::InicializarBarrio3(Puntero<ISistema> interfaz, bool ignOk)
{
	ignorarOK = ignOk;

	Array<pBarrio> barrios(5);
	// Orden creciente por direccion
	barrios[0] = new BarrioMock("Buceo", 10, 20, Iterador<pCliente>());
	barrios[1] = new BarrioMock("Centro", 40, 45, Iterador<pCliente>());
	barrios[2] = new BarrioMock("Cordon", 5, 40, Iterador<pCliente>());
	barrios[3] = new BarrioMock("Pocitos", 20, 10, Iterador<pCliente>());
	barrios[4] = new BarrioMock("Prado", 60, 60, Iterador<pCliente>());

	Cadena texto = "Se ingresa el barrio '{0}'.";


		for(int i = barrios.Largo - 1; i >= 0; i --){
			Verificar(interfaz->IngresoBarrio(barrios[i]->ObtenerNombreBarrio(), barrios[i]->ObtenercantidadCamaras(), barrios[i]->ObtenerpromedioHurtos()), OK, texto.DarFormato(barrios[i]->ObtenerNombreBarrio()));
		}

	interfaz->IngresoCliente(1048282, "Lucía", 20150523, "Prado", "Burgues 3921");
	interfaz->IngresoCliente(5, "Edgardo", 20150523, "Prado", "Joaquin Suarez 1234");
	interfaz->IngresoCliente(99999999, "Martin", 20150521, "Prado", "L A de Herrera 7112");
	interfaz->IngresoCliente(333, "Jose", 20150520, "Prado", "Millan 4532");

	interfaz->IngresoCliente(11111, "Robert", 20140123, "Pocitos", "Berro 1234");
	interfaz->IngresoCliente(2222, "Sebastian", 20130601, "Pocitos", "Ellauri 3921");
	interfaz->IngresoCliente(543112, "Ludwig", 20131111, "Pocitos", "Scoseria 4532");
	interfaz->IngresoCliente(324742, "Amadeus", 20001115, "Pocitos", "Tomas Diago 7112");

	ignorarOK = false;

	return barrios;
}

Array<pBarrio> CasoDePrueba::InicializarBarrio4(Puntero<ISistema> interfaz, bool ignOk)
{
	ignorarOK = ignOk;

	Array<pBarrio> barrios(5);
	// Orden creciente por direccion
	barrios[0] = new BarrioMock("Buceo", 10, 60, Iterador<pCliente>());
	barrios[1] = new BarrioMock("Centro", 40, 45, Iterador<pCliente>());
	barrios[2] = new BarrioMock("Cordon", 5, 40, Iterador<pCliente>());
	barrios[3] = new BarrioMock("Pocitos", 20, 10, Iterador<pCliente>());
	barrios[4] = new BarrioMock("Prado", 60, 60, Iterador<pCliente>());

	Cadena texto = "Se ingresa el barrio '{0}'.";


		for(int i = barrios.Largo - 1; i >= 0; i --){
			Verificar(interfaz->IngresoBarrio(barrios[i]->ObtenerNombreBarrio(), barrios[i]->ObtenercantidadCamaras(), barrios[i]->ObtenerpromedioHurtos()), OK, texto.DarFormato(barrios[i]->ObtenerNombreBarrio()));
		}
	ignorarOK = false;

	interfaz->IngresoCliente(1048282, "Lucía", 20150523, "Prado", "Burgues 3921");
	interfaz->IngresoCliente(5, "Edgardo", 20150523, "Prado", "Joaquin Suarez 1234");
	interfaz->IngresoCliente(99999999, "Martin", 20150521, "Prado", "L A de Herrera 7112");
	interfaz->IngresoCliente(333, "Jose", 20150520, "Prado", "Millan 4532");

	interfaz->IngresoCliente(11111, "Lionel", 20140123, "Buceo", "Buxareo 1234");
	interfaz->IngresoCliente(2222, "Wayne", 20130601, "Buceo", "L A de Herrera 3921");
	interfaz->IngresoCliente(543112, "Luis", 20131111, "Buceo", "26 de Marzo 4532");
	interfaz->IngresoCliente(324742, "Cristiano", 20001115, "Buceo", "Rivera 7112");

	return barrios;
}


void CasoDePrueba::InicializarRecorrido(Puntero<ISistema> interfaz, bool ignOk)
{
	//this -> InicializarCliente(interfaz, ignOk);
	ignorarOK = true;
	interfaz ->IngresoConexion("Pocitos", "Cordon", 10, 2);
	interfaz ->IngresoConexion("Cordon", "Pocitos", 10, 8);

	interfaz ->IngresoConexion("Pocitos", "Centro", 20, 10);
	interfaz ->IngresoConexion("Centro", "Pocitos", 20, 5);

	interfaz ->IngresoConexion("Cordon", "Buceo", 50, 2);

	interfaz ->IngresoConexion("Pocitos", "Buceo", 31, 6);

	interfaz ->IngresoConexion("Buceo", "Centro", 30, 3);

	interfaz ->IngresoConexion("Centro", "Prado", 35, 1);

	interfaz ->IngresoConexion("Cordon", "Centro", 80, 20);

	ignorarOK = false;
}

void CasoDePrueba::InicializarRecorrido2(Puntero<ISistema> interfaz, bool ignOk)
{
	//this -> InicializarCliente2(interfaz, ignOk);
	ignorarOK = true;
	interfaz ->IngresoConexion("Buceo", "Malvin", 47, 11);
	interfaz ->IngresoConexion("Cordon", "Malvin", 38, 8);

	interfaz ->IngresoConexion("Aires Puros", "Malvin", 18, 15);
	interfaz ->IngresoConexion("Malvin", "Aires Puros", 32, 15);

	interfaz ->IngresoConexion("Aires Puros", "Reducto", 33, 1);

	interfaz ->IngresoConexion("3 Ombues", "Aires Puros", 81, 5);

	interfaz ->IngresoConexion("Reducto", "3 Ombues", 7, 8);

	interfaz ->IngresoConexion("Reducto", "3 Ombues", 6, 0);

	interfaz ->IngresoConexion("Sayago", "3 Ombues", 1, 20);

	interfaz ->IngresoConexion("Prado", "Sayago", 63, 10);

	ignorarOK = false;
}

Cadena CasoDePrueba::GetNombre()const
{
	return "Casos de Prueba";
}

void CasoDePrueba::CorrerPruebaConcreta()
{
	PruebaOKIngresoBarrio();
	PruebaOKIngresoCliente();
	PruebaOKConsultaCliente();
	PruebaOKListadoBarrios();
	PruebaOKListadoClientes();
	PruebaOKIngresoConexion();
	PruebaOKCaminoMasRapido();
	PruebaOKBajaCliente();
	PruebaOKConsultaBarrio();
	PruebaOKBarrioConMasClientes();
	PruebaOKCaminoMenorDistancia();
	PruebaOKCableadoMinimo();

	PruebaERRORIngresoBarrio();
	PruebaERRORIngresoCliente();
	PruebaERRORConsultaCliente();
	PruebaERRORListadoBarrios();
	PruebaERRORListadoClientes();
	PruebaERRORIngresoConexion();
	PruebaERRORCaminoMasRapido();
	PruebaERRORBajaCliente();
	PruebaERRORConsultaBarrio();
	PruebaERRORBarrioConMasClientes();
	PruebaERRORCaminoMenorDistancia();
	PruebaERRORCableadoMinimo();
}

void CasoDePrueba::Verificar(TipoRetorno obtenido, TipoRetorno esperado, Cadena comentario)
{
	if (!ignorarOK || obtenido != esperado)
		Prueba::Verificar(obtenido, esperado, comentario);
}

template <class T>
void CasoDePrueba::Verificar(const T& obtenido, const T& esperado, Cadena comentario)
{
	Verificar(SonIguales(obtenido, esperado) ? OK : ERROR, OK, comentario.DarFormato(ObtenerTexto(obtenido), ObtenerTexto(esperado)));
}

template <class T>
void CasoDePrueba::VerificarConjuntos(Iterador<T> obtenidos, Iterador<T> esperados, Cadena comentarioEncontrado, Cadena comentarioFalta, Cadena comentarioSobra)
{
	bool verificarCantidad = true;
	nat totalObtenidos = 0;
	T aux;
	foreach (T obtenido, obtenidos)
	{
		totalObtenidos++;
		esperados.Reiniciar();
		if (Pertenece(obtenido, esperados, aux))
			Verificar(OK, OK, comentarioEncontrado);
		else
		{
			Verificar(ERROR, OK, comentarioSobra.DarFormato(ObtenerTexto(obtenido)));
			verificarCantidad = false;
		}
	}
	nat totalEsperados = 0;
	esperados.Reiniciar();
	foreach (T esperado, esperados)
	{
		totalEsperados++;
		obtenidos.Reiniciar();
		if (!Pertenece(esperado, obtenidos, aux))
		{
			Verificar(ERROR, OK, comentarioFalta.DarFormato(ObtenerTexto(esperado)));
			verificarCantidad = false;
		}
	}
	if (verificarCantidad && totalObtenidos != totalEsperados)
		Verificar(ERROR, OK, "Se verifica la cantidad de elementos de los conjuntos");
}

template <class T>
void CasoDePrueba::VerificarSecuencias(Iterador<T> obtenidos, Iterador<T> esperados, Cadena comentarioEncontrado, Cadena comentarioFalta, Cadena comentarioSobra)
{
	esperados.Reiniciar();

	foreach (T obtenido, obtenidos)
	{
		if (esperados.HayElemento())
		{
			T esperado = *esperados;
			++esperados;
			Verificar(obtenido, esperado, comentarioEncontrado);
		}
		else
			Verificar(ERROR, OK, comentarioSobra.DarFormato(ObtenerTexto(obtenido)));
	}

	while (esperados.HayElemento())
	{
		T esperado = *esperados;
		++esperados;
		Verificar(ERROR, OK, comentarioFalta.DarFormato(ObtenerTexto(esperado)));
	}
}

template <class T>
bool CasoDePrueba::SonIguales(Iterador<T> obtenidos, Iterador<T> esperados) const
{
	obtenidos.Reiniciar();
	esperados.Reiniciar();
	while (obtenidos.HayElemento() && esperados.HayElemento())
	{
		if (!SonIguales(*obtenidos, *esperados))
			return false;
		++obtenidos;
		++esperados;
	}

	return esperados.HayElemento() == obtenidos.HayElemento();
}

template <class T>
Cadena CasoDePrueba::ObtenerTexto(Iterador<T> it) const
{
	Cadena sepVacio = "";
	Cadena sepGuion = "-";
	Cadena sep = sepVacio;
	Cadena retorno = sepVacio;
	foreach (auto t, it)
	{
		retorno += sep + ObtenerTexto(t);
		sep = sepGuion;
	}
	return retorno;
}

template <class T>
bool CasoDePrueba::Pertenece(const T& dato, Iterador<T> iterador, T& encontrado) const
{
	foreach (T dato2, iterador)
	{
		if (SonIguales(dato, dato2))
		{
			encontrado = dato2;
			return true;
		}
	}
	return false;
}

void CasoDePrueba::VerificarConsultaCliente(Tupla<TipoRetorno, pCliente> obtenido, Tupla<TipoRetorno, pCliente> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		Verificar(obtenido.Dato2, esperado.Dato2, "Se obtuvo {0} y se esperaba {1}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

void CasoDePrueba::VerificarListadoBarrios(Tupla<TipoRetorno, Iterador<pBarrio>> obtenido, Tupla<TipoRetorno, Iterador<pBarrio>> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		VerificarSecuencias(obtenido.Dato2, esperado.Dato2, "Se obtuvo {0} y se esperaba {1}", "Se esperaba {0}", "No se esperaba {0}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

void CasoDePrueba::VerificarListadoClientes(Tupla<TipoRetorno, Iterador<pCliente>> obtenido, Tupla<TipoRetorno, Iterador<pCliente>> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		VerificarSecuencias(obtenido.Dato2, esperado.Dato2, "Se obtuvo {0} y se esperaba {1}", "Se esperaba {0}", "No se esperaba {0}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

void CasoDePrueba::VerificarCaminoMasRapido(Tupla<TipoRetorno, pRecorrido> obtenido, Tupla<TipoRetorno, pRecorrido> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		Verificar(obtenido.Dato2, esperado.Dato2, "Se obtuvo {0} y se esperaba {1}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

void CasoDePrueba::VerificarConsultaBarrio(Tupla<TipoRetorno, pBarrio> obtenido, Tupla<TipoRetorno, pBarrio> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		Verificar(obtenido.Dato2, esperado.Dato2, "Se obtuvo {0} y se esperaba {1}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

void CasoDePrueba::VerificarBarrioConMasClientes(Tupla<TipoRetorno, pBarrio> obtenido, Tupla<TipoRetorno, pBarrio> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		Verificar(obtenido.Dato2, esperado.Dato2, "Se obtuvo {0} y se esperaba {1}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

void CasoDePrueba::VerificarCaminoMenorDistancia(Tupla<TipoRetorno, pRecorrido> obtenido, Tupla<TipoRetorno, pRecorrido> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		Verificar(obtenido.Dato2, esperado.Dato2, "Se obtuvo {0} y se esperaba {1}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

void CasoDePrueba::VerificarCableadoMinimo(Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> obtenido, Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> esperado, Cadena comentario)
{
	if (obtenido.Dato1 == OK && esperado.Dato1 == OK)
	{
		IniciarSeccion(comentario, esperado.Dato1);
		Verificar(obtenido.Dato2 == esperado.Dato2 ? OK : ERROR, OK, "Se verifica el dato 2");
		VerificarConjuntos(obtenido.Dato3, esperado.Dato3, "Se obtuvo {0} y se esperaba {1}", "Se esperaba {0}", "No se esperaba {0}");
		CerrarSeccion();
	}
	else
		Verificar(obtenido.Dato1, esperado.Dato1, comentario);
}

bool CasoDePrueba::SonIguales(const pCliente& obtenido, const pCliente& esperado) const
{
	return obtenido->ObtenerCiCliente() == esperado->ObtenerCiCliente() && obtenido->ObtenerNombreCliente() == esperado->ObtenerNombreCliente() && obtenido->ObtenerFechaRegCliente() == esperado->ObtenerFechaRegCliente() && obtenido->ObtenerNombreBarrio() == esperado->ObtenerNombreBarrio() && obtenido->ObtenerDireccion() == esperado->ObtenerDireccion();
}

Cadena CasoDePrueba::ObtenerTexto(const pCliente& c) const
{
	return c->ObtenerNombreCliente();
}

bool CasoDePrueba::SonIguales(const pBarrio& obtenido, const pBarrio& esperado) const
{
	//TODO: Comprobar los clientes del barrio
	return obtenido->ObtenerNombreBarrio() == esperado->ObtenerNombreBarrio() && obtenido->ObtenercantidadCamaras() == esperado->ObtenercantidadCamaras() && obtenido->ObtenerpromedioHurtos() == esperado->ObtenerpromedioHurtos();
}

Cadena CasoDePrueba::ObtenerTexto(const pBarrio& b) const
{
	return b->ObtenerNombreBarrio();
}

bool CasoDePrueba::SonIguales(const pRecorrido& obtenido, const pRecorrido& esperado) const
{
	bool datosok = obtenido->ObtenerNombreBarrioOrigen() == esperado->ObtenerNombreBarrioOrigen() && obtenido->ObtenerDistanciaTotalRecorrido() == esperado->ObtenerDistanciaTotalRecorrido() && obtenido->ObtenerTiempoTotalRecorrido() == esperado->ObtenerTiempoTotalRecorrido();
	if (datosok)
	{
		return VerificarTraslados(obtenido->ObtenerTraslados(), esperado->ObtenerTraslados());
	}
	return datosok;
}

bool CasoDePrueba::VerificarTraslados(Iterador<Tupla<Cadena, nat, nat>> obtenidos, Iterador<Tupla<Cadena, nat, nat>> esperados) const
{
	esperados.Reiniciar();

	while (obtenidos.HayElemento())
	{
		Tupla<Cadena, nat, nat> obtenido = obtenidos.ElementoActual();
		if (esperados.HayElemento())
		{
			Tupla<Cadena, nat, nat> esperado = *esperados;
			++esperados;
			if (!SonIguales(obtenido, esperado))
			{
				return false;
			}
		}
		else
		{
			return false;
		}

		obtenidos.Avanzar();
	}

	while (esperados.HayElemento())
	{
		Tupla<Cadena, nat, nat> esperado = *esperados;
		++esperados;
		return false;
	}
	return true;
}


Cadena CasoDePrueba::ObtenerTexto(const pRecorrido& r) const
{
	return r->ObtenerNombreBarrioOrigen();
}

bool CasoDePrueba::SonIguales(const Tupla<Cadena, nat, nat>& obtenido, const Tupla<Cadena, nat, nat>& esperado) const
{
	return obtenido.Dato1 == esperado.Dato1 && obtenido.Dato2 == esperado.Dato2 && obtenido.Dato3 == esperado.Dato3;
}

Cadena CasoDePrueba::ObtenerTexto(const Tupla<Cadena, nat, nat>& t) const
{
	return t.Dato1;
}

bool CasoDePrueba::SonIguales(const Tupla<Cadena, Cadena, nat>& obtenido, const Tupla<Cadena, Cadena, nat>& esperado) const
{
	return obtenido.Dato3 == esperado.Dato3 && ((obtenido.Dato1 == esperado.Dato1 && obtenido.Dato2 == esperado.Dato2) || (obtenido.Dato1 == esperado.Dato2 && obtenido.Dato2 == esperado.Dato1));
}

Cadena CasoDePrueba::ObtenerTexto(const Tupla<Cadena, Cadena, nat>& t) const
{
	return t.Dato1 + " - " + t.Dato2 + " : "; //itoa +t.Dato3;
}

void CasoDePrueba::PruebaOKIngresoBarrio()
{
	IniciarSeccion("Ingreso Barrio");
	Puntero<ISistema> interfaz = InicializarSistema(100,50);

	InicializarBarrio(interfaz);
	InicializarBarrio2(interfaz);
	CerrarSeccion();
}

void CasoDePrueba::PruebaOKIngresoCliente()
{
	IniciarSeccion("Ingreso Cliente");
	Puntero<ISistema> interfaz = InicializarSistema(100, 50);
	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarCliente(interfaz);
	CerrarSeccion();
}

void CasoDePrueba::PruebaOKConsultaCliente()
{
	IniciarSeccion("Consulta Cliente");
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, pCliente> esperado;
	Tupla<TipoRetorno, pCliente> obtenido;

	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	Array<pCliente> clientes = this -> InicializarCliente(interfaz, true);

	obtenido = interfaz->ConsultaCliente(5);
	esperado = Tupla<TipoRetorno, pCliente>(OK, new ClienteMock(5, "Edgardo", 20150523, "Prado", "Joaquin Suarez 1234"));

	VerificarConsultaCliente(obtenido, esperado, "Se verifica a al cliente CI 5.");

	obtenido = interfaz->ConsultaCliente(47);
	esperado = Tupla<TipoRetorno, pCliente>(OK, new ClienteMock(47, "Juan", 20150904, "Centro", "Cuareim 1517"));

	VerificarConsultaCliente(obtenido, esperado, "Se verifica a al cliente CI 47.");

	obtenido = interfaz->ConsultaCliente(333);
	esperado = Tupla<TipoRetorno, pCliente>(OK, new ClienteMock(333, "Jose", 20150520, "Prado", "Millan 4532"));

	VerificarConsultaCliente(obtenido, esperado, "Se verifica a al cliente CI 333.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaOKListadoBarrios()
{
	IniciarSeccion("Listado Barrios");
	Puntero<ISistema> interfaz = InicializarSistema();

	Array<pBarrio> solucion = this->InicializarBarrio(interfaz, true);

	Tupla<TipoRetorno, Iterador<pBarrio>> esperado;
	Tupla<TipoRetorno, Iterador<pBarrio>> obtenido;

	obtenido = interfaz->ListadoBarrios();
	esperado = Tupla<TipoRetorno, Iterador<pBarrio>>(OK, solucion.ObtenerIterador());

	VerificarListadoBarrios(obtenido, esperado, "Se listan los barrios ordenados por nombre.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaOKListadoClientes()
{
	IniciarSeccion("Listado Clientes");
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, Iterador<pCliente>> esperado;
	Tupla<TipoRetorno, Iterador<pCliente>> obtenido;

	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);

	esperado = Tupla<TipoRetorno, Iterador<pCliente>>(OK, this->InicializarCliente2(interfaz, true).ObtenerIterador());

	obtenido = interfaz->ListadoClientes();

	VerificarListadoClientes(obtenido, esperado, "Se listan los clientes ordenados alfabeticamente.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaOKIngresoConexion()
{
	IniciarSeccion("Ingreso Conexion");
	Puntero<ISistema> interfaz = InicializarSistema();
	this->InicializarBarrio(interfaz, true);
	Verificar(interfaz->IngresoConexion("Prado","Centro", 10, 6), OK, "Se ingresa conexion Prado-Centro.");
	Verificar(interfaz->IngresoConexion("Pocitos","Cordon", 5, 10), OK, "Se ingresa conexion Pocitos-Cordon.");
	Verificar(interfaz->IngresoConexion("Buceo","Cordon", 20, 12), OK, "Se ingresa conexion Buceo-Cordon.");
	Verificar(interfaz->IngresoConexion("Centro","Cordon", 5, 3), OK, "Se ingresa conexion Centro-Cordon.");
	Verificar(interfaz->IngresoConexion("Cordon","Centro", 10, 1), OK, "Se ingresa conexion Cordon-Centro.");
	Verificar(interfaz->IngresoConexion("Centro","Prado", 12, 20), OK, "Se ingresa conexion Centro-Prado.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaOKCaminoMasRapido()
{
	IniciarSeccion("Camino Mas Rapido");
	Puntero<ISistema> interfaz = InicializarSistema();

	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarCliente(interfaz, true);
	InicializarRecorrido(interfaz, true);

	Tupla<TipoRetorno, pRecorrido> esperado;
	Tupla<TipoRetorno, pRecorrido> obtenido;

	obtenido = interfaz->CaminoMasRapido("Horacio", "Edgardo");
	Array<Tupla<Cadena, nat, nat>> recorrido(3);
	recorrido[0] = Tupla<Cadena, nat, nat>("Buceo", 50, 2);
	recorrido[1] = Tupla<Cadena, nat, nat>("Centro", 30, 3);
	recorrido[2] = Tupla<Cadena, nat, nat>("Prado", 35, 1);
	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Cordon", recorrido.ObtenerIterador(), 115, 6));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino mas rapido desde Horacio hasta Edgardo.");

	obtenido = interfaz->CaminoMasRapido("Maria", "Martín");
	Array<Tupla<Cadena, nat, nat>> recorrido2(4);
	recorrido2[0] = Tupla<Cadena, nat, nat>("Cordon", 10, 2);
	recorrido2[1] = Tupla<Cadena, nat, nat>("Buceo", 50, 2);
	recorrido2[2] = Tupla<Cadena, nat, nat>("Centro", 30, 3);
	recorrido2[3] = Tupla<Cadena, nat, nat>("Prado", 35, 1);
	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Pocitos", recorrido2.ObtenerIterador(), 125, 8));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino mas rapido desde Maria hasta Martin.");

	interfaz = InicializarSistema();
	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarCliente(interfaz, true);
	InicializarRecorrido(interfaz, true);
	InicializarRecorrido2(interfaz, true);
	obtenido = interfaz->CaminoMasRapido("Maria", "Zoraida");
	Array<Tupla<Cadena, nat, nat>> recorrido3(5);
	recorrido3[0] = Tupla<Cadena, nat, nat>("Cordon", 10, 2);
	recorrido3[1] = Tupla<Cadena, nat, nat>("Malvin", 38, 8);
	recorrido3[2] = Tupla<Cadena, nat, nat>("Aires Puros", 32, 15);
	recorrido3[3] = Tupla<Cadena, nat, nat>("Reducto", 33, 1);
	recorrido3[4] = Tupla<Cadena, nat, nat>("3 Ombues", 6, 0);
	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Pocitos", recorrido3.ObtenerIterador(), 119, 26));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino mas rapido desde Maria hasta Zoraida.");

	obtenido = interfaz->CaminoMasRapido("Horacio", "Zoraida");
	Array<Tupla<Cadena, nat, nat>> recorrido4(4);
	recorrido4[0] = Tupla<Cadena, nat, nat>("Malvin", 38, 8);
	recorrido4[1] = Tupla<Cadena, nat, nat>("Aires Puros", 32, 15);
	recorrido4[2] = Tupla<Cadena, nat, nat>("Reducto", 33, 1);
	recorrido4[3] = Tupla<Cadena, nat, nat>("3 Ombues", 6, 0);
	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Cordon", recorrido4.ObtenerIterador(), 109, 24));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino mas rapido desde Horacio hasta Zoraida.");

	obtenido = interfaz->CaminoMasRapido("Maria", "Juan");
	Array<Tupla<Cadena, nat, nat>> recorrido5(3);
	recorrido5[0] = Tupla<Cadena, nat, nat>("Cordon", 10,2);
	recorrido5[1] = Tupla<Cadena, nat, nat>("Buceo", 50, 2);
	recorrido5[2] = Tupla<Cadena, nat, nat>("Centro", 30, 3);
	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Pocitos", recorrido5.ObtenerIterador(), 90, 7));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino mas rapido desde Maria hasta Juan.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaOKBajaCliente()
{
	IniciarSeccion("Baja Cliente");
	Puntero<ISistema> interfaz = InicializarSistema();
	InicializarBarrio(interfaz, true);

	Verificar(interfaz->BajaCliente(5), OK, "Se da de baja al cliente CI 5.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaOKConsultaBarrio()
{
	IniciarSeccion("Consulta Barrio");
	Puntero<ISistema> interfaz = InicializarSistema();

	Array<pBarrio> barrios = this->InicializarBarrio3(interfaz, true);

	Tupla<TipoRetorno, pBarrio> esperado;
	Tupla<TipoRetorno, pBarrio> obtenido;

	// Prado
	obtenido = interfaz->ConsultaBarrio("Prado");
	esperado = Tupla<TipoRetorno, pBarrio>(OK, barrios[4]);
	Array<pCliente> cPrado(4);
	cPrado[0] = new  ClienteMock(1048282, "Lucía", 20150523, "Prado", "Burgues 3921");
	cPrado[1] = new  ClienteMock(5, "Edgardo", 20150523, "Prado", "Joaquin Suarez 1234");
	cPrado[2] = new  ClienteMock(99999999, "Martin", 20150521, "Prado", "L A de Herrera 7112");
	cPrado[3] = new  ClienteMock(333, "Jose", 20150520, "Prado", "Millan 4532");

	VerificarConsultaBarrio(obtenido, esperado, "Se consulta el barrio Prado.");
	Iterador<pCliente> it = obtenido.Dato2->ObtenerClientesPorDireccion();
	VerificarListadoClientes(Tupla<TipoRetorno, Iterador<pCliente>>(OK,obtenido.Dato2->ObtenerClientesPorDireccion()), Tupla<TipoRetorno, Iterador<pCliente>>(OK,cPrado.ObtenerIterador()), "Se consultan los clientes ordenados por dirección");

	// Pocitos
	obtenido = interfaz->ConsultaBarrio("Pocitos");
	esperado = Tupla<TipoRetorno, pBarrio>(OK, barrios[3]);

	Array<pCliente> cPocitos(4);

	cPocitos[0] = new ClienteMock(11111, "Robert", 20140123, "Pocitos", "Berro 1234");
	cPocitos[1] = new ClienteMock(2222, "Sebastian", 20130601, "Pocitos", "Ellauri 3921");
	cPocitos[2] = new ClienteMock(543112, "Ludwig", 20131111, "Pocitos", "Scoseria 4532");
	cPocitos[3] = new ClienteMock(324742, "Amadeus", 20001115, "Pocitos", "Tomas Diago 7112");



	VerificarConsultaBarrio(obtenido, esperado, "Se consulta el barrio Pocitos.");
	VerificarListadoClientes(Tupla<TipoRetorno, Iterador<pCliente>>(OK,obtenido.Dato2->ObtenerClientesPorDireccion()), Tupla<TipoRetorno, Iterador<pCliente>>(OK,cPocitos.ObtenerIterador()), "Se consultan los clientes ordenados por dirección");


	// Buceo
	interfaz = InicializarSistema();
	barrios = this->InicializarBarrio4(interfaz, true);

	obtenido = interfaz->ConsultaBarrio("Buceo");
	esperado = Tupla<TipoRetorno, pBarrio>(OK, barrios[0]);

	Array<pCliente> cBuceo(4);
	cBuceo[0] = new ClienteMock(543112, "Luis", 20131111, "Buceo", "26 de Marzo 4532");
	cBuceo[1] = new ClienteMock(11111, "Lionel", 20140123, "Buceo", "Buxareo 1234");
	cBuceo[2] = new ClienteMock(2222, "Wayne", 20130601, "Buceo", "L A de Herrera 3921");
	cBuceo[3] = new ClienteMock(324742, "Cristiano", 20001115, "Buceo", "Rivera 7112");

	VerificarConsultaBarrio(obtenido, esperado, "Se consulta el barrio Buceo.");
	VerificarListadoClientes(Tupla<TipoRetorno, Iterador<pCliente>>(OK,obtenido.Dato2->ObtenerClientesPorDireccion()), Tupla<TipoRetorno, Iterador<pCliente>>(OK,cBuceo.ObtenerIterador()), "Se consultan los clientes ordenados por dirección");

	CerrarSeccion();
}

void CasoDePrueba::PruebaOKBarrioConMasClientes()
{
	IniciarSeccion("Barrio Con Mas Clientes 1");
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, pBarrio> esperado;
	Tupla<TipoRetorno, pBarrio> obtenido;

	Array<pBarrio> barrios = InicializarBarrio(interfaz, true);

	obtenido = interfaz->BarrioConMasClientes();
	pBarrio barrioEsperado = barrios[4];

	esperado = Tupla<TipoRetorno, pBarrio>(OK, barrioEsperado); // Prado

	VerificarBarrioConMasClientes(obtenido, esperado, "Se consulta el barrio con mas clientes.");

	CerrarSeccion();

	IniciarSeccion("Barrio Con Mas Clientes 2");
	interfaz = InicializarSistema();
	
	barrios = InicializarBarrio3(interfaz, true);

	obtenido = interfaz->BarrioConMasClientes();
    barrioEsperado = barrios[3];

	esperado = Tupla<TipoRetorno, pBarrio>(OK, barrioEsperado); // Pocitos - mismos clientes menos hurtos

	VerificarBarrioConMasClientes(obtenido, esperado, "Se consulta el barrio con mas clientes.");

	CerrarSeccion();

	IniciarSeccion("Barrio Con Mas Clientes 3");
	interfaz = InicializarSistema();
	
	barrios = InicializarBarrio4(interfaz, true);

	obtenido = interfaz->BarrioConMasClientes();
	barrioEsperado = barrios[0];

	esperado = Tupla<TipoRetorno, pBarrio>(OK, barrioEsperado); // Buceo - mismos clientes pero alfabeticamente antes

	VerificarBarrioConMasClientes(obtenido, esperado, "Se consulta el barrio con mas clientes.");

	CerrarSeccion();

}


void CasoDePrueba::PruebaOKCaminoMenorDistancia()
{
	IniciarSeccion("Camino Menor Distancia");
	Puntero<ISistema> interfaz = InicializarSistema();
	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarCliente(interfaz, true);
	InicializarRecorrido(interfaz, true);

	Tupla<TipoRetorno, pRecorrido> esperado;
	Tupla<TipoRetorno, pRecorrido> obtenido;

	obtenido = interfaz->CaminoMenorDistancia("Pocitos","Cordon");
	Array<Tupla<Cadena, nat, nat>> recorrido(1);
	recorrido[0] = Tupla<Cadena, nat, nat>("Cordon", 10, 2);

	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Pocitos", recorrido.ObtenerIterador(), 10, 2));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino de menor distancia desde Pocitos hasta Cordon.");

	obtenido = interfaz->CaminoMenorDistancia("Cordon", "Prado");
	Array<Tupla<Cadena, nat, nat>> recorrido2(3);
	recorrido2[0] = Tupla<Cadena, nat, nat>("Pocitos", 10, 8);
	recorrido2[1] = Tupla<Cadena, nat, nat>("Centro", 20, 10);
	recorrido2[2] = Tupla<Cadena, nat, nat>("Prado", 35, 1);

	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Cordon", recorrido2.ObtenerIterador(), 65, 19));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino de menor distancia desde Cordon hasta Prado.");

	interfaz = InicializarSistema();
	InicializarBarrio(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarRecorrido(interfaz, true);
	InicializarRecorrido2(interfaz, true);

	obtenido = interfaz->CaminoMenorDistancia("Pocitos", "Aires Puros");
	Array<Tupla<Cadena, nat, nat>> recorrido3(3);
	recorrido3[0] = Tupla<Cadena, nat, nat>("Cordon", 10, 2);
	recorrido3[1] = Tupla<Cadena, nat, nat>("Malvin", 38,8);
	recorrido3[2] = Tupla<Cadena, nat, nat>("Aires Puros", 32, 15);

	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Pocitos", recorrido3.ObtenerIterador(), 80, 25));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino de menor distancia desde Pocitos hasta Aires Puros.");


	obtenido = interfaz->CaminoMenorDistancia("Buceo", "Reducto");
	Array<Tupla<Cadena, nat, nat>> recorrido4(3);
	recorrido4[0] = Tupla<Cadena, nat, nat>("Malvin", 47, 11);
	recorrido4[1] = Tupla<Cadena, nat, nat>("Aires Puros", 32,15);
	recorrido4[2] = Tupla<Cadena, nat, nat>("Reducto", 33, 1);

	esperado = Tupla<TipoRetorno, pRecorrido>(OK, new RecorridoMock("Buceo", recorrido4.ObtenerIterador(), 112, 27));

	VerificarCaminoMasRapido(obtenido, esperado, "Se lista el camino de menor distancia desde Buceo hasta Reducto.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaOKCableadoMinimo()
{
	IniciarSeccion("Cableado Minimo");
	Puntero<ISistema> interfaz = InicializarSistema();
	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarCliente2(interfaz, true);
	this -> InicializarRecorrido(interfaz, true);

	Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> esperado;
	Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> obtenido;

	Array<Tupla<Cadena, Cadena, nat>> sol(4);
	sol[0] = Tupla<Cadena, Cadena, nat>("Cordon", "Pocitos", 10);
	sol[1] = Tupla<Cadena, Cadena, nat>("Pocitos", "Centro", 20);
	sol[2] = Tupla<Cadena, Cadena, nat>("Centro", "Prado", 35);
	sol[3] = Tupla<Cadena, Cadena, nat>("Buceo", "Centro", 30);


	obtenido = interfaz->CableadoMinimo();
	esperado = Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>>(OK, 95, sol.ObtenerIterador());

	VerificarCableadoMinimo(obtenido, esperado, "Comentario");

	// PRUEBA 2
	this -> InicializarRecorrido2(interfaz, true);
	Array<Tupla<Cadena, Cadena, nat>> sol2(9);
	sol2[0] = Tupla<Cadena, Cadena, nat>("Cordon", "Pocitos", 10);
	sol2[1] = Tupla<Cadena, Cadena, nat>("Pocitos", "Centro", 20);
	sol2[2] = Tupla<Cadena, Cadena, nat>("Centro", "Prado", 35);
	sol2[3] = Tupla<Cadena, Cadena, nat>("Buceo", "Centro", 30);
	sol2[4] = Tupla<Cadena, Cadena, nat>("Cordon", "Malvin", 38);
	sol2[5] = Tupla<Cadena, Cadena, nat>("Malvin", "Aires Puros", 32);
	sol2[6] = Tupla<Cadena, Cadena, nat>("Aires Puros", "Reducto", 33);
	sol2[7] = Tupla<Cadena, Cadena, nat>("Reducto", "3 Ombues", 6);
	sol2[8] = Tupla<Cadena, Cadena, nat>("Sayago", "3 Ombues", 1);

	obtenido = interfaz->CableadoMinimo();
	esperado = Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>>(OK, 205, sol2.ObtenerIterador());

	VerificarCableadoMinimo(obtenido, esperado, "Comentario");

	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORIngresoBarrio()
{
	IniciarSeccion("Ingreso Barrio", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema(6);
	InicializarBarrio(interfaz, true);
	Verificar(interfaz->IngresoBarrio("Centro", 40, 450), ERROR, "ERROR: Ya existe el barrio de nombre Centro.");
	Verificar(interfaz->IngresoBarrio("Pocitos", 30, 10), ERROR, "ERROR: Ya existe el barrio de nombre Pocitos.");	
	Verificar(interfaz->IngresoBarrio("PocitosNuevo", 30, 10), OK, "OK. Se alcanza el máximo");
	Verificar(interfaz->IngresoBarrio("Bronx", 230,10), ERROR, "ERROR: Se sobrepasa la cantidad máxima de barrios.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORIngresoCliente()
{
	IniciarSeccion("Ingreso Cliente", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema(20,16);
	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarCliente(interfaz, true);

	Verificar(interfaz->IngresoCliente(5, "Gerardo", 20151203, "Centro", "18 de Julio 342"), ERROR, "ERROR: Ya existe el cliente con CI 5.");
	Verificar(interfaz->IngresoCliente(1331122, "Mario", 20151203, "Bronx", "5th Avenue 2031"), ERROR, "No existe el barrio con nombre Bronx.");
	Verificar(interfaz->IngresoCliente(84631, "Romulo", 20151203, "Centro", "18 de Julio 342"), OK, "Se agrega a Romulo.");
	Verificar(interfaz->IngresoCliente(1991122, "Ester", 20150701, "Centro", "Uruguay 1291"), ERROR, "ERROR: Se sobrepasa la cantidad máxima de clientes.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORConsultaCliente()
{
	IniciarSeccion("Consulta Cliente", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, pCliente> esperado;
	Tupla<TipoRetorno, pCliente> obtenido;
	//InicializarCliente(interfaz, true);

	obtenido = interfaz->ConsultaCliente(9999);
	esperado = Tupla<TipoRetorno, pCliente>(ERROR, nullptr);

	VerificarConsultaCliente(obtenido, esperado, "No existe el cliente con CI 9999.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORListadoBarrios()
{
	IniciarSeccion("Listado Barrios", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();
	Tupla<TipoRetorno, Iterador<pBarrio>> esperado;
	Tupla<TipoRetorno, Iterador<pBarrio>> obtenido;

	esperado = Tupla<TipoRetorno, Iterador<pBarrio>>(ERROR, nullptr);
	obtenido = interfaz->ListadoBarrios();

	VerificarListadoBarrios(obtenido, esperado, "No hay barrios ingresados en el sistema.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORListadoClientes()
{
	IniciarSeccion("Listado Clientes", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, Iterador<pCliente>> esperado;
	Tupla<TipoRetorno, Iterador<pCliente>> obtenido;

	esperado = Tupla<TipoRetorno, Iterador<pCliente>>(ERROR, nullptr);
	obtenido = interfaz->ListadoClientes();

	VerificarListadoClientes(obtenido, esperado, "No hay clientes ingresados en el sistema.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORIngresoConexion()
{
	IniciarSeccion("Ingreso Conexion", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();
	InicializarBarrio(interfaz, true);

	Verificar(interfaz->IngresoConexion("Prado","Bronx", 10, 6), ERROR, "No existe el barrio destino con nombre Bronx.");
	Verificar(interfaz->IngresoConexion("Bronx","Pocitos", 90, 10), ERROR, "No existe el barrio origen con nombre Bronx.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORCaminoMasRapido()
{
	IniciarSeccion("Camino Mas Rapido", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, pRecorrido> esperado;
	Tupla<TipoRetorno, pRecorrido> obtenido;

	//InicializarCliente(interfaz, true);

	obtenido = interfaz->CaminoMasRapido("Federica", "Tomas");
	esperado = Tupla<TipoRetorno, pRecorrido>(ERROR, nullptr);

	VerificarCaminoMasRapido(obtenido, esperado, "No existe ni Federica ni Tomas.");

	interfaz->IngresoBarrio("Centro", 10, 10);
	interfaz->IngresoBarrio("Prado", 1, 10);
	interfaz->IngresoCliente(0,"Federica",20011509,"Centro","pp");

	obtenido = interfaz->CaminoMasRapido("Federica", "Tomas");

	VerificarCaminoMasRapido(obtenido, esperado, "No existe Tomas.");

	interfaz->IngresoCliente(1,"Tomas",20011509,"Prado","pp");

	obtenido = interfaz->CaminoMasRapido("Federica", "Tomas");

	VerificarCaminoMasRapido(obtenido, esperado, "No existe camino de Federcia a Tomas.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORBajaCliente()
{
	IniciarSeccion("Baja Cliente", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();

	Verificar(interfaz->BajaCliente(44381239), ERROR, "No existe un cliente con CI 44381239.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORConsultaBarrio()
{
	IniciarSeccion("Consulta Barrio", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, pBarrio> esperado;
	Tupla<TipoRetorno, pBarrio> obtenido;

	InicializarBarrio(interfaz, true);

	obtenido = interfaz->ConsultaBarrio("Soho");
	esperado = Tupla<TipoRetorno, pBarrio>(ERROR, nullptr);

	VerificarConsultaBarrio(obtenido, esperado, "No existe el barrio con nombre Soho.");

	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORBarrioConMasClientes()
{
	IniciarSeccion("Barrio Con Mas Clientes", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, pBarrio> esperado;
	Tupla<TipoRetorno, pBarrio> obtenido;

	obtenido = interfaz->BarrioConMasClientes();
	esperado = Tupla<TipoRetorno, pBarrio>(ERROR, nullptr);

	VerificarBarrioConMasClientes(obtenido, esperado, "No hay barrios ingresados en el sistema.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORCaminoMenorDistancia()
{
	IniciarSeccion("Camino Menor Distancia", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();

	Tupla<TipoRetorno, pRecorrido> esperado;
	Tupla<TipoRetorno, pRecorrido> obtenido;

	this->InicializarBarrio5(interfaz, true);

	this->InicializarRecorrido(interfaz, true);

	obtenido = interfaz->CaminoMenorDistancia("Prado", "Cordon");
	esperado = Tupla<TipoRetorno, pRecorrido>(ERROR, nullptr);

	VerificarCaminoMasRapido(obtenido, esperado, "No existe camino entre Prado hasta Cordon.");

	obtenido = interfaz->CaminoMenorDistancia("Buceo", "Bronx");
	esperado = Tupla<TipoRetorno, pRecorrido>(ERROR, nullptr);

	VerificarCaminoMasRapido(obtenido, esperado, "No existe el barrio Bronx.");
	CerrarSeccion();
}

void CasoDePrueba::PruebaERRORCableadoMinimo()
{
	IniciarSeccion("Cableado Minimo", ERROR);
	Puntero<ISistema> interfaz = InicializarSistema();
	InicializarBarrio5(interfaz, true);
	InicializarBarrio2(interfaz, true);
	InicializarRecorrido(interfaz, true);
	InicializarRecorrido2(interfaz, true);

	Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> obtenido;
	Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>> esperado;

	InicializarCliente(interfaz, true);
	obtenido = Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>>(ERROR, interfaz->CableadoMinimo().Dato2);
	esperado = Tupla<TipoRetorno, nat, Iterador<Tupla<Cadena, Cadena, nat>>>(ERROR, 0, nullptr);

	VerificarCableadoMinimo(obtenido, esperado, "Comentario");

	CerrarSeccion();
}
