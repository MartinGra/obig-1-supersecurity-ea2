#ifndef HASH_H
#define HASH_H
#include "Array.h"
#include "Puntero.h"
#include "FuncionHash.h"
#include "Lista.h"
template <class T>
class Hash {
public:
	Hash(nat, Puntero<FuncionHash<T>>);
	~Hash(){
		f = NULL;
		arrH = NULL;
	}
	void Add(nat ind, const T& t);
	bool Delete(T&);
	T&Recover(const T) const;
	nat h(const T& dato) const;
	bool isMember(const T&)const;
	Array<Puntero<Lista<T>>>getArray(){ return arrH; }
	void printHash();
	int getCantElementos()const{
		return cantidadElementos;
	}
	Hash& operator=(const Hash& h) {
		f = h.f;
		arrH.Copiar(h.arrH, arrH, NULL);
		celdasOcupadas = h.celdasOcupadas;
		cantidadElementos = h.cantidadElementos;
		factorCarga = h.factorCarga;
	};
private:
	Puntero<FuncionHash<T>> f;
	Array<Puntero<Lista<T>>> arrH;
	int celdasOcupadas;
	int cantidadElementos;
	const double factorCarga = 1.10; // No queremos que crezca
	int nextPrime(int d);
	//void grow();
	
};

#include "Hash.cpp"
#endif
