#ifndef COMPARADORCLIENTEPORDIRECCION_H
#define COMPARADORCLIENTEPORDIRECCION_H
#include "Puntero.h"
#include "Cliente.h"
#include "Comparacion.h"
class ComparadorClientePorDireccion : public Comparacion < Puntero<Cliente> > {

	CompRetorno Comparar(const Puntero<Cliente> &c1, const Puntero<Cliente> &c2) const{
		if (c1->ObtenerDireccion() < c2->ObtenerDireccion()) return MAYOR;
		if (c1->ObtenerDireccion() > c2->ObtenerDireccion()) return MENOR;
		if (c1->ObtenerDireccion() == c2->ObtenerDireccion()) return IGUALES;
		else return DISTINTOS;
	}
};

#endif