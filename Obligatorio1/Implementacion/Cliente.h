#ifndef CLIENTE_H
#define CLIENTE_H

#include "ICliente.h"

class Cliente :public ICliente{

private:
	nat cedula;
	Cadena nombre;
	nat fechaReg;
	Cadena barrio;
	Cadena direccion;

public:
	
	Cliente(nat unaC,Cadena unN, nat unaF, Cadena unB, Cadena unaD){
		cedula = unaC;
		nombre = unN;
		fechaReg = unaF;
		barrio = unB;
		direccion = unaD;
	}
	Cliente(const Cliente& c){
		cedula = c.cedula;
		nombre = c.nombre;
		fechaReg = c.fechaReg;
		barrio = c.barrio;
		direccion = c.direccion;
	}
	Cliente(){
		cedula = 0;
		nombre = "";
		fechaReg = 0;
		barrio = "";
		direccion = "";
	}

	~Cliente(){}

	nat ObtenerCiCliente() const override{
		return cedula;
	};
	Cadena ObtenerNombreCliente() const override{
		return nombre;
	};
	nat ObtenerFechaRegCliente() const override{
		return fechaReg;
	};
	Cadena ObtenerNombreBarrio() const override{
		return barrio;
	};
	Cadena ObtenerDireccion() const override{
		return direccion;
	};

	bool operator==(const ICliente& c) const override{
		return ObtenerCiCliente() == c.ObtenerCiCliente();
	};

	bool operator==(const Cliente& c) const {
		return cedula == c.ObtenerCiCliente();
	};

	Cliente& operator=(const Cliente& c) {
		if (this != &c){
			cedula = c.ObtenerCiCliente();
			nombre = c.ObtenerNombreCliente();
			fechaReg = c.ObtenerFechaRegCliente();
			barrio = c.ObtenerNombreBarrio();
			direccion = c.ObtenerDireccion();
		}
		return *this;
	};

	friend ostream& operator<<(ostream& out, const Cliente& c){
		out <<"( "<< c.ObtenerCiCliente()<<" - "<< c.ObtenerNombreCliente()<<" )";
		return out;
	}

	bool operator<(const Cliente& c) const{
		return ObtenerNombreCliente() < c.ObtenerNombreCliente();
	}
	bool operator>(const Cliente& c) const{
		return ObtenerNombreCliente() > c.ObtenerNombreCliente();
	}
	bool operator<=(const Cliente& c) const{
		return ObtenerNombreCliente() <= c.ObtenerNombreCliente();
	}
	bool operator>=(const Cliente& c) const{
		return ObtenerNombreCliente() >= c.ObtenerNombreCliente();
	}
};

#endif