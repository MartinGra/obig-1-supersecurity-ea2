#ifndef COLAPRIORIDAD_H
#define COLAPRIORIDAD_H
#include "Puntero.h"
#include "Array.h"
#include "nodoCP.h"
template<class P, class T>
class CP{
public:
	virtual ~CP(){};
	//pre
	//pos retorna cola vacia
	virtual Puntero<CP<P, T>> CrearColaVacia() const abstract;
	//pre
	//pos retorna una copia de this que no comparte memoria
	virtual Puntero<CP<P, T>> Clon() const abstract;
	//pre
	//pos esVacia()
	virtual void Vacia() abstract;
	//pre
	//pos retorna true si la cola no tiene elementos
	virtual bool EsVacia() const abstract;
	//pre
	//pos retorna true si la cola esta llena
	virtual bool EstaLlena() const { return false;}
	//pre !estaLlena()
	//pos encola T en Prioridad P
	virtual void Encolar(P &p, T &t) abstract; // 
	//pre !esVacia()
	//pos elimina el elemento de mayor(menor) prioridad,pone igual prioridad
	virtual void Desencolar() abstract;
	//pre !esVacia()
	//pos retorna el elemento de mayor(menor) prioridad
	virtual T Frente() const abstract;
	//pre !esVacia
	//pos retorna la prioridad del frente de la cola
	virtual P PrioridadFrente() const abstract;

	virtual nat getTope() abstract; //importo array para solucionar tipos
	virtual Array<Puntero<nodoCP<P, T>>> getHeap() abstract;
	//pre
	//pos retorna true sii this esta contenida en "c"
	virtual bool operator<(const CP<P, T> &c) const abstract;
	//pre
	//pos retorna true sii this y "c" tienen los mismos elementos en el mismo orden
	virtual bool operator==(const CP<P, T> &c) const abstract;
	//pre
	//pos *this == c && no comparten memoria || a = b = c
	//virtual CP<P, T>& operator=(const CP<P, T> &c) abstract;
};
#endif