#ifndef NODOAVL_H
#define NODOAVL_H

#include "Puntero.h"

template <class T>
class NodoAVL
{
	typedef Puntero<NodoAVL> pNodoAvl;

public:
	~NodoAVL(){
		hIzq = NULL;
		hDer = NULL;
	}

	NodoAVL(){
		this->dato = NULL;
		this->fb = 0;
		this->hDer = this->hIzq = NULL;
	}
	NodoAVL(const T& dato)
	{
		this->dato = dato;
		this->fb = 0;
		this->hDer = this->hIzq = NULL;
	}

	NodoAVL(const T& dato, pNodoAvl izq, pNodoAvl der)
	{
		this->dato = dato;
		this->fb = 0;
		this->hIzq = izq;
		this->hDer = der;
	}
	//No los vamos a usar, sino que utilizamos una clase propia que hereda de la clase comparador.
	bool operator==(const NodoAVL<T>& nodo) { return this == &nodo; }
	bool operator<(const NodoAVL<T>& nodo) { return this < &nodo; }
	bool operator>(const NodoAVL<T>& nodo) { return this > &nodo; }
	

	T dato;
	pNodoAvl hIzq;
	pNodoAvl hDer;
	int fb;
};


#endif