#ifndef GRAFOIMPMATRIZ_CPP
#define GRAFOIMPMATRIZ_CPP
#include "grafoImpMatriz.h"


template<class V,class A>
void GrafoImpMatriz<V, A>::agregarVertice(V v){
	//nat pos = tablaVaNat->natHash(v); Antes se usaba pos en lugar de cantVertices para hacer esto dado que es un hash, no un array
	/* Cambiar tabla de nat a V, para que contenga lista de V, y al momento de obtenerlos que sea buscar en la posicion del bucket el Vertice que verifique el nombre*/
	tablaVaNat->Agregar(v, cantVertices);
	tablaNataV->Agregar(cantVertices, v);
	cantVertices++;
}
template<class V, class A>
void GrafoImpMatriz<V, A>::agregarArista(A a, V orig, V dest){
	//nat posOrigen = tablaVaNat->natHash(orig);
	//nat posDestino = tablaVaNat->natHash(dest); cuidado esta es la forma de usar hash anterior al cambio de incie con cant vertices
	nat posOrigen = tablaVaNat->Recuperar(orig);
	nat posDestino = tablaVaNat->Recuperar(dest);

	if (matrizAdy[posOrigen][posDestino]) matrizAdy[posOrigen][posDestino]->Insertar(a); //podriamos fijarnos si existe ya la arista, nose con que fin pero...
	else matrizAdy[posOrigen][posDestino] = new Lista<A>(a); 
			
	cantAristas++;
}

template<class V, class A>
bool GrafoImpMatriz<V, A>::existeVertice(int posV) const{
	if (posV == -1) return false; //este es el indice designado a elementos que no estan en la tabla, no tienen posicion en ella
	else return tablaNataV->Recuperar(posV) != NULL;
	//return tablaNataV->EstaDef(posV); esto es lo legal hacer con hash, pero se usa una mezcla tipo array en este caso
}

template<class V, class A>
int GrafoImpMatriz<V, A>::getCantVertices(){
	return cantVertices;
}
template<class V, class A>
int GrafoImpMatriz<V, A>::getMaxVertices(){
	return maxVertices;
}
template<class V, class A>
int GrafoImpMatriz<V, A>::getCantAristas(){
	return cantAristas;
}
template<class V, class A>
V GrafoImpMatriz<V, A>::getVertice(int i){
	return tablaNataV->Recuperar(i);
}
template<class V, class A>
int GrafoImpMatriz<V, A>::getVertice(V v){
	if (tablaVaNat->EstaDef(v)){
		return tablaVaNat->Recuperar(v);
	}
	else{
		return -1;
	}
}
template<class V, class A>
Iterador<A> GrafoImpMatriz<V, A>::aristas(V origen, V destino){
	nat posOrigen = tablaVaNat->natHash(origen);
	nat posDestino = tablaVaNat->natHash(destino);
	if (matrizAdy[posOrigen][posDestino]){
		return (matrizAdy[posOrigen][posDestino])->ObtenerIterador();
	}
	else{
		return NULL;
	}
}

template<class V, class A>
Iterador<V> GrafoImpMatriz<V, A>::adyacentes(V origen){
	Puntero<Lista<V>> vertices = new Lista<V>();
	nat posOrigen = getVertice(origen);
	if (existeVertice(origen)){
		nat i = 0;
		while (i < maxVertices){
			if (matrizAdy[posOrigen][i]){//origen conectado con getVertice(i)
				V ad = getVertice(i);
				vertices->Insertar(ad);
			}
			i++;
		}
		return vertices->ObtenerIterador();
	}
	else return NULL;
}
template<class V,class A>
V GrafoImpMatriz<V, A>::getOrigen(A a){
	V verticeOrigen = NULL;
	for (nat i = 0; i < maxVertices; i++)
		for (nat j = 0; j < maxVertices; j++)
			if (matrizAdy[i][j]){
				if (matrizAdy[i][j]->Pertenece(a)) verticeOrigen = getVertice(i);
				
			}
	return verticeOrigen;
}
template<class V, class A>
V GrafoImpMatriz<V, A>::getDestino(A a) {
	V verticeOrigen = NULL;
	for (nat i = 0; i < maxVertices; i++) for (nat j = 0; j < maxVertices; j++) if (matrizAdy[i][j]) if (matrizAdy[i][j]->Pertenece(a)) verticeOrigen = getVertice(j); 
	return verticeOrigen;
}

template<class V, class A>
Puntero<Grafo<V,A>> GrafoImpMatriz<V, A>::Clon() const{
	Puntero<GrafoImpMatriz<V, A>> ret;
	ret->maxVertices = maxVertices;
	ret->cantVertices = cantVertices;
	ret->cantAristas = cantAristas;
	ret->tablaVaNat = tablaVaNat->Clon();
	ret->tablaNataV = tablaNataV->Clon();
	ret->matrizAdy = matrizAdy;
	return ret;
}
template<class V, class A>
Iterador<A> GrafoImpMatriz<V, A>::getTodasAristas(){
	Puntero<Lista<A>> listaRet;
	for (nat i = 0; i < maxVertices; i++)
		for (nat j = 0; j < maxVertices; j++)
			if (matrizAdy[i][j]){
				Iterador<A> itTemp = matrizAdy[i][j]->ObtenerIterador();
				while (itTemp.HayElemento()){
					listaRet->Insertar(itTemp.ElementoActual());
					itTemp.Avanzar();
				}
			}
	return listaRet->ObtenerIterador();	
}

#endif